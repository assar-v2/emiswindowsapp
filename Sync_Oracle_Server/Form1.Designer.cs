﻿namespace Sync_Oracle_Server
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button3 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.EndDate = new System.Windows.Forms.DateTimePicker();
            this.StartDate = new System.Windows.Forms.DateTimePicker();
            this.DateSelection = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.btnGenerateTxtFile = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btn_Daily_NavPrice = new System.Windows.Forms.Button();
            this.btn_fund_distribution = new System.Windows.Forms.Button();
            this.btn_Fund_Information = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_refersh = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(307, 57);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(133, 60);
            this.button3.TabIndex = 2;
            this.button3.Text = "Select-Day";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 17);
            this.label2.TabIndex = 18;
            this.label2.Text = "End_Date :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 17);
            this.label1.TabIndex = 17;
            this.label1.Text = "Start_Date :";
            // 
            // EndDate
            // 
            this.EndDate.CustomFormat = "yyyy/MM/dd";
            this.EndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.EndDate.Location = new System.Drawing.Point(100, 95);
            this.EndDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.EndDate.Name = "EndDate";
            this.EndDate.Size = new System.Drawing.Size(200, 22);
            this.EndDate.TabIndex = 16;
            // 
            // StartDate
            // 
            this.StartDate.CustomFormat = "yyyy/MM/dd";
            this.StartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.StartDate.Location = new System.Drawing.Point(100, 57);
            this.StartDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.StartDate.Name = "StartDate";
            this.StartDate.Size = new System.Drawing.Size(200, 22);
            this.StartDate.TabIndex = 15;
            // 
            // DateSelection
            // 
            this.DateSelection.AutoSize = true;
            this.DateSelection.ForeColor = System.Drawing.Color.Red;
            this.DateSelection.Location = new System.Drawing.Point(57, 136);
            this.DateSelection.Name = "DateSelection";
            this.DateSelection.Size = new System.Drawing.Size(0, 17);
            this.DateSelection.TabIndex = 35;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(448, 57);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(169, 60);
            this.button1.TabIndex = 36;
            this.button1.Text = "DB_Connection";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(25, 174);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(651, 365);
            this.tabControl1.TabIndex = 43;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.checkBox3);
            this.tabPage1.Controls.Add(this.checkBox4);
            this.tabPage1.Controls.Add(this.checkBox2);
            this.tabPage1.Controls.Add(this.checkBox1);
            this.tabPage1.Controls.Add(this.button10);
            this.tabPage1.Controls.Add(this.button9);
            this.tabPage1.Controls.Add(this.button8);
            this.tabPage1.Controls.Add(this.button7);
            this.tabPage1.Controls.Add(this.button6);
            this.tabPage1.Controls.Add(this.btnGenerateTxtFile);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(643, 336);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Text file for eMIS ";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(377, 248);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(84, 21);
            this.checkBox3.TabIndex = 56;
            this.checkBox3.Text = "Option 2";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.Visible = false;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(377, 211);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(135, 21);
            this.checkBox4.TabIndex = 55;
            this.checkBox4.Text = "Historical Report";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Checked = true;
            this.checkBox2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox2.Location = new System.Drawing.Point(377, 83);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(156, 21);
            this.checkBox2.TabIndex = 52;
            this.checkBox2.Text = "No Zipping required";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(377, 33);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(129, 21);
            this.checkBox1.TabIndex = 51;
            this.checkBox1.Text = "Zip and encrypt";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(28, 264);
            this.button10.Margin = new System.Windows.Forms.Padding(4);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(217, 37);
            this.button10.TabIndex = 50;
            this.button10.Text = "Fund Size";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(28, 202);
            this.button9.Margin = new System.Windows.Forms.Padding(4);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(217, 37);
            this.button9.TabIndex = 49;
            this.button9.Text = "Fund Daily NAV";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(28, 141);
            this.button8.Margin = new System.Windows.Forms.Padding(4);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(217, 37);
            this.button8.TabIndex = 48;
            this.button8.Text = "Fund Income Distribution";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(28, 83);
            this.button7.Margin = new System.Windows.Forms.Padding(4);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(217, 37);
            this.button7.TabIndex = 47;
            this.button7.Text = "Member UTMC";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(28, 22);
            this.button6.Margin = new System.Windows.Forms.Padding(4);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(217, 40);
            this.button6.TabIndex = 46;
            this.button6.Text = "Fund Info";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // btnGenerateTxtFile
            // 
            this.btnGenerateTxtFile.Location = new System.Drawing.Point(377, 129);
            this.btnGenerateTxtFile.Margin = new System.Windows.Forms.Padding(4);
            this.btnGenerateTxtFile.Name = "btnGenerateTxtFile";
            this.btnGenerateTxtFile.Size = new System.Drawing.Size(200, 60);
            this.btnGenerateTxtFile.TabIndex = 45;
            this.btnGenerateTxtFile.Text = "Final_EXPORT";
            this.btnGenerateTxtFile.UseVisualStyleBackColor = true;
            this.btnGenerateTxtFile.Click += new System.EventHandler(this.btnGenerateTxtFile_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button5);
            this.tabPage2.Controls.Add(this.button4);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.btn_Daily_NavPrice);
            this.tabPage2.Controls.Add(this.btn_fund_distribution);
            this.tabPage2.Controls.Add(this.btn_Fund_Information);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(643, 336);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Sync Oracle";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(318, 196);
            this.button5.Margin = new System.Windows.Forms.Padding(4);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(252, 60);
            this.button5.TabIndex = 52;
            this.button5.Text = "Acc Holder Details";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click_1);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(318, 107);
            this.button4.Margin = new System.Windows.Forms.Padding(4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(252, 60);
            this.button4.TabIndex = 51;
            this.button4.Text = "Member Invesment";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(318, 27);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(252, 60);
            this.button2.TabIndex = 50;
            this.button2.Text = "Member Transactions";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btn_Daily_NavPrice
            // 
            this.btn_Daily_NavPrice.Location = new System.Drawing.Point(58, 196);
            this.btn_Daily_NavPrice.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Daily_NavPrice.Name = "btn_Daily_NavPrice";
            this.btn_Daily_NavPrice.Size = new System.Drawing.Size(252, 60);
            this.btn_Daily_NavPrice.TabIndex = 49;
            this.btn_Daily_NavPrice.Text = "Daily NAV Price";
            this.btn_Daily_NavPrice.UseVisualStyleBackColor = true;
            this.btn_Daily_NavPrice.Click += new System.EventHandler(this.btn_Daily_NavPrice_Click);
            // 
            // btn_fund_distribution
            // 
            this.btn_fund_distribution.Location = new System.Drawing.Point(58, 107);
            this.btn_fund_distribution.Margin = new System.Windows.Forms.Padding(4);
            this.btn_fund_distribution.Name = "btn_fund_distribution";
            this.btn_fund_distribution.Size = new System.Drawing.Size(252, 60);
            this.btn_fund_distribution.TabIndex = 48;
            this.btn_fund_distribution.Text = "Fund Distribution";
            this.btn_fund_distribution.UseVisualStyleBackColor = true;
            this.btn_fund_distribution.Click += new System.EventHandler(this.btn_fund_distribution_Click);
            // 
            // btn_Fund_Information
            // 
            this.btn_Fund_Information.Location = new System.Drawing.Point(58, 27);
            this.btn_Fund_Information.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Fund_Information.Name = "btn_Fund_Information";
            this.btn_Fund_Information.Size = new System.Drawing.Size(252, 60);
            this.btn_Fund_Information.TabIndex = 47;
            this.btn_Fund_Information.Text = "Fund Information";
            this.btn_Fund_Information.UseVisualStyleBackColor = true;
            this.btn_Fund_Information.Click += new System.EventHandler(this.btn_Fund_Information_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_refersh);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.DateSelection);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.StartDate);
            this.groupBox1.Controls.Add(this.EndDate);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(9, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(704, 156);
            this.groupBox1.TabIndex = 44;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Date Selection";
            // 
            // btn_refersh
            // 
            this.btn_refersh.BackColor = System.Drawing.Color.Red;
            this.btn_refersh.Location = new System.Drawing.Point(625, 57);
            this.btn_refersh.Margin = new System.Windows.Forms.Padding(4);
            this.btn_refersh.Name = "btn_refersh";
            this.btn_refersh.Size = new System.Drawing.Size(59, 60);
            this.btn_refersh.TabIndex = 37;
            this.btn_refersh.Text = "Refersh";
            this.btn_refersh.UseVisualStyleBackColor = false;
            this.btn_refersh.Visible = false;
            this.btn_refersh.Click += new System.EventHandler(this.btn_refersh_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(912, 551);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "eMIS-ASSAR v1.6";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker EndDate;
        private System.Windows.Forms.DateTimePicker StartDate;
        private System.Windows.Forms.Label DateSelection;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button btnGenerateTxtFile;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btn_Daily_NavPrice;
        private System.Windows.Forms.Button btn_fund_distribution;
        private System.Windows.Forms.Button btn_Fund_Information;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_refersh;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
    }
}

