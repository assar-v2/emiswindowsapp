﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Sync_Oracle_Server.WEBService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISyncOracle" in both code and config file together.
    [ServiceContract]
    public interface ISyncOracle
    {
        [OperationContract]
        string DoWork();
        [OperationContract]
        DataTable Get_FundInfo();
        [OperationContract]
        DataTable Get_NAV_Per_Fund(string startDate,string EndDate);
        [OperationContract]
        DataTable Get_Corporate_Actions();
        [OperationContract]
        DataTable Get_FundInfo_Details();
        [OperationContract]
        DataTable GenerateDataTable(string Query,string DatabaseType);
       
        
    }
}
