﻿using MySql.Data.MySqlClient;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Sync_Oracle_Server.WEBService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SyncOracle" in both code and config file together.
    public class SyncOracle : ISyncOracle
    {
        DataManager DB = new DataManager();

        public string DoWork()
        {
            return "OK";
        }

        public DataTable Get_FundInfo()
        {
            DataTable ds = new DataTable();

                string Query = "SELECT A.FUND_ID,A.FUND_TYPE,A.L_NAME,B.EPF_APPROVE FROM UTS.FUND A "
                                + " INNER JOIN UTS.FUND_POLICY B ON A.FUND_ID = B.FUND_ID "
                                + " WHERE A.FUND_ID IN('01', '02', '03', '04', '05', '06', '07', '08', '09')"
                                 + " ORDER BY A.FUND_ID ASC";
                ds = GenerateDataTable(Query, "Oracle");
                return ds;

            }

        public DataTable Get_NAV_Per_Fund(string StartDate,string EndDate)
        {
            DataTable ds = new DataTable();
                string Query = " SELECT A.FUND_ID, A.TRANS_DT, A.UP_SELL, A.UNIT_IN_ISSUE, A.UP_SELL * A.UNIT_IN_ISSUE "
                               + "  FROM UTS.PRICE_HISTORY A INNER JOIN "
                               + "  (SELECT FUND_ID, TRANS_DT, MAX(SEQ_NO)AS MaxRecord FROM UTS.PRICE_HISTORY GROUP BY FUND_ID, TRANS_DT) B "
                               + "  ON A.FUND_ID = B.FUND_ID AND A.TRANS_DT = B.TRANS_DT AND A.SEQ_NO = B.MaxRecord "
                               + "  WHERE A.FUND_ID IN ('AS') AND "
                               + "  (A.trans_dt between to_date('"+StartDate+"', 'yyyy/mm/dd') "
                               + "  and to_date('"+EndDate+"', 'yyyy/mm/dd'))  AND A.PR_STATUS = 'C' "
                               + "  ORDER BY A.FUND_ID, A.TRANS_DT ASC ";

                ds = GenerateDataTable(Query, "Oracle");
                return ds;
            }




        public DataTable Get_Corporate_Actions()
        {
           
            DataTable ds = new DataTable();

                string Query = "  SELECT FUND_ID, DIV_RT , distn_dt ,NET_DIV_RT,reinv_dt FROM UTS.NEW_DIV_DISTN"
                                + "  WHERE "
                                 + "   FUND_ID IN('AS') "
                                  + "   ORDER BY FUND_ID ";
                ds = GenerateDataTable(Query, "Oracle");
                return ds;
        }

        public DataTable Get_FundInfo_Details()
        {
            DataTable ds = new DataTable();

          //

            string Query = " select A.FUND_ID,A.UNIT_CREATN_MIN,A.UNIT_CREATN_MAX,A.REINV_AMT_MIN,A.REINV_AMT_MAX,A.UNIT_HLDG_MAX,A.UNIT_HLDG_MIN,A.EPF_APPROVE, "
                            + " B.COMMENCE_DT,B.CURRENCY,B.UP_SELL,A.subseq_sale_min,A.INITIAL_SALE_MIN,A.COOLING_PRD,B.NO_OF_HOLDER,B.CASH_AVAIL from UTS.FUND_POLICY A inner join  UTS.FUND B "
                            + " on A.FUND_ID = B.FUND_ID where A.fund_id in ('01','02','03','04','05','06','07','08','09') order by Fund_ID ";

           ds =GenerateDataTable(Query, "Oracle");
            return ds;
        }

        public DataTable Get_FundInfo_Details_emis()
        {
            DataTable ds = new DataTable();

            //

            string Query = " select B.L_NAME,B.L_NAME,"
                              + "  A.FUND_ID,A.UNIT_CREATN_MIN,A.SUBSEQ_SALE_MIN,"
                               + " A.REINV_AMT_MIN,A.UNIT_HLDG_MIN,B.L_NAME"
                              + "  from UTS.FUND_POLICY A inner join  UTS.FUND B"
                              + "  on A.FUND_ID = B.FUND_ID where A.fund_id in ('AS')"
                              + "  order by Fund_ID";

            ds = GenerateDataTable(Query, "Oracle");
            return ds;
        }



        public DataTable Get_Member_Transactions(string Start_date,string End_date,string BirthDateupdate)
        {
            DataTable ds = new DataTable();



            string Query = "select  A.FUND_ID, "
                                    + " B.ID_NO_2 AS EPF_NO, "
                                    + " A.HOLDER_NO, "
                                    + " A.TRANS_DT, "
                                    + " A.TRANS_NO, "
                                    + " A.TRANS_TYPE, "
                                    + " A.ACC_TYPE, "
                                    + " A.TRANS_UNITS, "
                                    + " A.TRANS_AMT,  "
                                    + " A.FEE_AMT, "
                                    + " A.GST_AMT,  "
                                    + " A.UNIT_VALUE, "
                                    + " (A.GST_AMT+ A.UNIT_VALUE) AS GAIN, C.RCPT_DT AS SETTLEMENT_DT,A.HLDR_AVE_COST, "
                                     + " A.SORT_SEQ "
                                    + " from UTS.HOLDER_LEDGER A "
                                    + " INNER JOIN UTS.HOLDER_REG B "
                                    + " ON A.HOLDER_NO  = B.HOLDER_NO "
                                    + " LEFT JOIN UTS.UTSTRANS C"
                                    + " ON A.TRANS_NO = C.TRANS_NO "
                                    + " AND A.TRANS_DT = C.TRANS_DT "
                                    + " where "
                                    + "(A.trans_dt between to_date('" + Start_date + "', 'yyyy/mm/dd')"
                                    + " and to_date('" + End_date + "', 'yyyy/mm/dd'))"
                                    + " AND A.FUND_ID IN ('05', '01', '02', '03', '04', '06', '07')"
                                    + " AND B.HOLDER_CLS IN ('EB','EN','ER','ES','EZ')"
                                    + " AND B.BIRTH_DT > to_date('" + BirthDateupdate + "', 'yyyy/mm/dd') " //adjust only display active member for historical
                                    + " AND NOT A.TRANS_TYPE IN ('UC', 'CO')"
                                    + " ORDER BY A.TRANS_TYPE, ACC_TYPE, A.FUND_ID, A.TRANS_DT ASC ";

            string Query1 = "select * from UTS.HOLDER_LEDGER A where trans_dt between to_date('" + Start_date + "', 'yyyy/mm/dd')  and to_date('" + End_date + "', 'yyyy/mm/dd')";
                                   

            ds = GenerateDataTable(Query1, "Oracle");
            return ds;
        }

        public DataTable Get_RedumptionCost(string Holder_No, string Fund_ID, string End_date)
        {
            DataTable ds = new DataTable();

            string Query = "SELECT TRANS_NO, TRANS_TYPE, ACC_TYPE, ENTRY_DT, TRANS_DT,"
                                  + " TRANS_UNITS, TRANS_AMT AS BIG,"
                                  + " (TRANS_UNITS * TRANS_PR) AS SMALL, CUR_UNIT_HLDG, CUM_VALUE_HLDG, SORT_SEQ"
                                  + " FROM UTS.HOLDER_LEDGER "
                                  + " WHERE "
                                  + " HOLDER_NO='" + Holder_No + "' AND "
                                  + " FUND_ID='" + Fund_ID + "' AND "
                                  + " trans_type IN ('SA','RD','DD','BI','UC', 'TR') "
                                  // + " trans_type IN ('SA','RD','DD','TR') "
                                  + " AND  TRANS_DT <= to_date('" + End_date + "', 'yyyy/mm/dd') "
                                  + " ORDER BY TRANS_DT ASC,SORT_SEQ ASC, TRANS_TYPE DESC ";


            ds = GenerateDataTable(Query, "Oracle");
            return ds;
        }

        public DataTable Get_Member_Invesment(string NAV_PricePU_Dt,string TransDateUpdate, string  BirthDateupdate)
        {

            DataTable ds = new DataTable();
            string Query = "select DISTINCT S3.ID_NO_2 AS EPF_NO, S1.FUND_ID,s1.HOLDER_NO, S1.TRANS_UNITS,S1.TRANS_AMT, "
                     +" s1.TRANS_DT, s1.CUR_UNIT_HLDG, S3.ID_NO, s3.BIRTH_DT, S1.TRANS_TYPE,S1.TRANS_PR, (S4.UP_SELL * s1.CUR_UNIT_HLDG) AS MARKET_VALUE "
                     + " from UTS.HOLDER_LEDGER s1 inner "
                     + " join UTS.HOLDER_REG s3 on s1.HOLDER_NO = s3.HOLDER_NO "
                      + "  INNER JOIN UTS.PRICE_HISTORY S4 ON S1.FUND_ID = S4.FUND_ID AND S4.TRANS_DT = (select max(TRANS_DT) from UTS.PRICE_HISTORY "
                       + "  where TRANS_DT = to_date('"+NAV_PricePU_Dt+"', 'yyyy/mm/dd') AND PR_STATUS = 'C' "
                     + " group by fund_id) AND S4.PR_STATUS = 'C' INNER JOIN  (select max(SORT_SEQ) SORT_SEQ, MAX(TRANS_DT) TRANS_DT, FUND_ID, "
                       + "  HOLDER_NO   from UTS.HOLDER_LEDGER WHERE TRANS_DT < to_date('"+TransDateUpdate+"', 'yyyy/mm/dd') group by HOLDER_NO, FUND_ID    ) s2 "
                      + "    on s1.HOLDER_NO = s2.HOLDER_NO  and s1.SORT_SEQ = s2.SORT_SEQ  where "
                     // + " s1.CUR_UNIT_HLDG > 0 AND "
                      + " s3.HOLDER_CLS IN ('BE') "
                        + " AND S3.BIRTH_DT > to_date('"+BirthDateupdate+"', 'yyyy/mm/dd')  AND "
                        +" S1.FUND_ID IN('AS') AND s1.TRANS_DT < to_date('"+TransDateUpdate+"', 'yyyy/mm/dd') "
                         + "   ORDER BY S1.FUND_ID, S1.HOLDER_NO ";

            ds = GenerateDataTable(Query, "Oracle");
            return ds;
        }

        public DataTable Get_Member_Invesment_Histrocial(string NAV_PricePU_Dt, string TransDateUpdate, string BirthDateupdate)
        {

            DataTable ds = new DataTable();
            string Query = "select DISTINCT S3.ID_NO_2 AS EPF_NO, S1.FUND_ID,s1.HOLDER_NO, S1.TRANS_UNITS,S1.TRANS_AMT, "
                     + " s1.TRANS_DT, s1.CUR_UNIT_HLDG, S3.ID_NO, s3.BIRTH_DT, S1.TRANS_TYPE,S1.TRANS_PR, (S4.UP_SELL * s1.CUR_UNIT_HLDG) AS MARKET_VALUE "
                     + " from UTS.HOLDER_LEDGER s1 inner "
                     + " join UTS.HOLDER_REG s3 on s1.HOLDER_NO = s3.HOLDER_NO "
                      + "  INNER JOIN UTS.PRICE_HISTORY S4 ON S1.FUND_ID = S4.FUND_ID AND S4.TRANS_DT = (select max(TRANS_DT) from UTS.PRICE_HISTORY "
                       + "  where TRANS_DT >= to_date('" + NAV_PricePU_Dt + "', 'yyyy/mm/dd') AND TRANS_DT <= to_date('" + TransDateUpdate + "', 'yyyy/mm/dd') and PR_STATUS = 'C' "
                     + " group by fund_id) AND S4.PR_STATUS = 'C' INNER JOIN  (select max(SORT_SEQ) SORT_SEQ, MAX(TRANS_DT) TRANS_DT, FUND_ID, "
                       + "  HOLDER_NO   from UTS.HOLDER_LEDGER WHERE TRANS_DT < to_date('" + TransDateUpdate + "', 'yyyy/mm/dd') group by HOLDER_NO, FUND_ID    ) s2 "
                      + "    on s1.HOLDER_NO = s2.HOLDER_NO  and s1.SORT_SEQ = s2.SORT_SEQ  where "
                       + " s1.CUR_UNIT_HLDG > 0 AND "
                      + " s3.HOLDER_CLS IN ('BE') "
                        + " AND S3.BIRTH_DT > to_date('"+BirthDateupdate+"', 'yyyy/mm/dd')  AND "
                        + " S1.FUND_ID IN('AS') AND s1.TRANS_DT < to_date('" + TransDateUpdate + "', 'yyyy/mm/dd') "
                         + "   ORDER BY S1.FUND_ID, S1.HOLDER_NO ";

            ds = GenerateDataTable(Query, "Oracle");
            return ds;
        }



        public DataTable Get_Holder_Acc_Details(string StartDate)
        {
            
            DataTable ds = new DataTable();

            string Query = " SELECT A1.* FROM   UTS.HOLDER_REG A1 WHERE A1.HOLDER_NO IN "
         +" (SELECT s3.HOLDER_NO   from UTS.HOLDER_LEDGER s1 inner "
         + "    join UTS.HOLDER_REG s3 on s1.HOLDER_NO = s3.HOLDER_NO "
         + " INNER JOIN(select max(SORT_SEQ) SORT_SEQ, MAX(TRANS_DT) TRANS_DT, FUND_ID, HOLDER_NO "
         + " from UTS.HOLDER_LEDGER WHERE TRANS_DT < to_date('"+StartDate+"', 'yyyy/mm/dd') "
         + "  group by HOLDER_NO, FUND_ID) s2  on s1.HOLDER_NO = s2.HOLDER_NO  and s1.SORT_SEQ = s2.SORT_SEQ where "
      //   + "  s1.CUR_UNIT_HLDG > 0 AND "
     +"    s3.HOLDER_CLS IN('BE') "
         + "  AND S3.BIRTH_DT > to_date('1962-12-31', 'yyyy/mm/dd')  AND S1.FUND_ID "
         + "  IN('AS') AND s1.TRANS_DT < to_date('"+StartDate+"', 'yyyy/mm/dd') "
         + " AND NOT S1.HOLDER_NO IN('9504', '19765') "  
         + " GROUP BY S3.HOLDER_NO) ORDER BY A1.REG_DT  " ;


            ds = GenerateDataTable(Query, "Oracle");
            return ds;
        }

        public DataTable Get_Holder_Acc_Details_Histrocial(string StartDate)
        {

            DataTable ds = new DataTable();

            string Query = " SELECT A1.* FROM   UTS.HOLDER_REG A1 WHERE A1.HOLDER_NO IN "
         + " (SELECT s3.HOLDER_NO   from UTS.HOLDER_LEDGER s1 inner "
         + "    join UTS.HOLDER_REG s3 on s1.HOLDER_NO = s3.HOLDER_NO "
         + " INNER JOIN(select max(SORT_SEQ) SORT_SEQ, MAX(TRANS_DT) TRANS_DT, FUND_ID, HOLDER_NO "
         + " from UTS.HOLDER_LEDGER WHERE TRANS_DT < to_date('" + StartDate + "', 'yyyy/mm/dd') "
         + "  group by HOLDER_NO, FUND_ID) s2  on s1.HOLDER_NO = s2.HOLDER_NO  and s1.SORT_SEQ = s2.SORT_SEQ where "
       + "  s1.CUR_UNIT_HLDG > 0 AND "
     + "    s3.HOLDER_CLS IN('BE') "
         + "  AND S3.BIRTH_DT > to_date('1962-12-31', 'yyyy/mm/dd')  AND S1.FUND_ID "
         + "  IN('AS') AND s1.TRANS_DT < to_date('" + StartDate + "', 'yyyy/mm/dd') "
         + " AND NOT S1.HOLDER_NO IN('9504', '19765') "
         + " GROUP BY S3.HOLDER_NO) ORDER BY A1.REG_DT  ";


            ds = GenerateDataTable(Query, "Oracle");
            return ds;
        }



        public DataTable GetHolder_ledger()
        {
            DataTable ds = new DataTable();

            string Query = "select * from holder_ledger";


            ds = GenerateDataTable(Query, "Oracle");
            return ds;
        }

        public DataTable GenerateDataTable(string Query,string DataBaseType)
        {


            DataTable ds = new DataTable();

            if (DataBaseType == "Oracle")
            {
                string ConnectionString = DB.GetOracleConnection();
                OracleConnection conn = new OracleConnection(ConnectionString);
                try
                {
                    string mysql = Query;

                    OracleCommand cmd = new OracleCommand(mysql, conn);
                    conn.Open();
                    cmd.CommandType = CommandType.Text;
                    OracleDataAdapter da = new OracleDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                }
                catch (Exception ex)
                {
                   Log.Write("GenerateDataTable : "+ ex.ToString(),Convert.ToDateTime(DateTime.Now).ToString("yyy-MM-dd"));
                }
                finally
                {
                    conn.Close();
                }
            }
            else if(DataBaseType == "MySQL")
            {
                 string ConnectionString = DB.GetSqlConnection();
                MySqlConnection conn = new MySqlConnection(ConnectionString);
                try
                {
                    string mysql = Query;

                    MySqlCommand cmd = new MySqlCommand(mysql, conn);
                    conn.Open();
                    cmd.CommandType = CommandType.Text;
                    MySqlDataAdapter da = new MySqlDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                }
                catch (Exception ex)
                {
                    Log.Write("GenerateDataTable : " + ex.ToString(), Convert.ToDateTime(DateTime.Now).ToString("yyy-MM-dd"));
                }
                finally
                {
                    conn.Close();
                }
            }
            return ds;

        }

    }
}

