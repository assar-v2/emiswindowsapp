﻿using ComponentAce.Compression.Archiver;
using ComponentAce.Compression.ZipForge;
using MySql.Data.MySqlClient;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sync_Oracle_Server
{
    public partial class Form1 : Form
    {

        DataManager DB = new DataManager();
        Sync_Oracle_Server.WEBService.SyncOracle CallService = new WEBService.SyncOracle();
        DataTable DS = new DataTable();
        string Start_date = "";
        string End_date = "";

        string TransDateUpdate = "";
        string BirthDateupdate = "";




        public Form1()
        {
            InitializeComponent();
        }


        public void InitTimer()
        {
            timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Interval = 10000;
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            string CheckSchedulerTime = dt.ToString("hh:mm:tt");
            string SchedulerTime = ConfigurationManager.AppSettings["SchedulerTime"];

            if (CheckSchedulerTime == SchedulerTime)
            {
                Log.Write("STOP" + DateTime.Now.ToString(), End_date);

                //function 

                timer1.Stop();
            }

            Log.Write("start" + DateTime.Now.ToString(), End_date);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Inser_Fund_Nav();


            OpenFileDialog fDialog = new OpenFileDialog();
            fDialog.Title = "Select file to be upload";
            fDialog.Filter = "(*.xls)|*.xls";
            if (fDialog.ShowDialog() == DialogResult.OK)
            {
                string Name = fDialog.FileName.ToString();
            }

        }

        protected void Insert_Corporate_Actions()
        {
            DS = CallService.Get_Corporate_Actions();
            string Sqlconnstring = DB.GetSqlConnection();
            using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
            {
                mysql_conn.Open();
                foreach (DataRow rdr in DS.Rows)
                {
                    string EPF_IPD_Code = ConfigurationManager.AppSettings["IPD_CODE"];
                    string IPD_Fund_Code = rdr[0].ToString();
                    DateTime dt = Convert.ToDateTime(rdr[2].ToString());
                    string Corporate_Action_Date = dt.ToString("yyyy-MM-dd");

                    var firstDayOfMonth = new DateTime(dt.Year, dt.Month, 1);
                    var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                    string End_date = lastDayOfMonth.ToString("yyyy-MM-dd");

                    decimal Distributions = Convert.ToDecimal(rdr[1].ToString());
                    Distributions = decimal.Round(Distributions, 4);
                    string Unit_Splits ="";

                    decimal Net_Distrubutions = Convert.ToDecimal(rdr[3].ToString());
                    Net_Distrubutions = decimal.Round(Net_Distrubutions, 4);

                    DateTime dt1 = Convert.ToDateTime(rdr[4].ToString());
                    string ReinstatementDate = dt1.ToString("yyyy-MM-dd");


                    string Query = "Insert into utmc_fund_corporate_actions(EPF_IPD_Code,IPD_Fund_Code,Corporate_Action_Date,Distributions,Unit_Splits,Report_Date,net_distribution)"
                   + " values('" + EPF_IPD_Code + "','" + IPD_Fund_Code + "','" + Corporate_Action_Date + "','" + Distributions + "','" + Unit_Splits + "','" + ReinstatementDate + "','" + Net_Distrubutions + "')";
                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                    {

                        string Value = DB.Find_Value_exists("select * from utmc_fund_corporate_actions where Corporate_Action_Date='" + Corporate_Action_Date + "' and IPD_Fund_Code='" + IPD_Fund_Code + "'");
                        if (Value == "0")
                        {
                            mysql_comm.ExecuteNonQuery();
                            Log.Write("Sync Corporate Actions : " + IPD_Fund_Code + "-" + IPD_Fund_Code + "-" + Corporate_Action_Date + " Inserted", DateTime.Now.ToString("yyyy-MM-dd"));
                        }
                    }
                }
                mysql_conn.Close();
            }
        }

        protected void Insert_Fund_Info()
        {

            DS = CallService.Get_FundInfo();
            string Sqlconnstring = DB.GetSqlConnection();
            using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
            {
                mysql_conn.Open();
                foreach (DataRow rdr in DS.Rows)
                {
                    string EPF_IPD_Code = ConfigurationManager.AppSettings["IPD_CODE"];
                    string IPD_Fund_Code = rdr[0].ToString();
                    string Fund_Name = rdr[2].ToString();
                    string Status = rdr[3].ToString().Trim();
                    if (Status == "Y") Status = "Active";
                    else Status = "Suspended";



                    string Query = "INSERT INTO utmc_fund_information(EPF_IPD_Code,IPD_Fund_Code,Fund_Name,Status)"
                            + " values('" + EPF_IPD_Code + "','" + IPD_Fund_Code + "','" + Fund_Name + "','" + Status + "')";


                    // string Query1="INSERT Into "

                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                    {
                        string Value = DB.Find_Value_exists("select * from utmc_fund_information where IPD_Fund_Code='" + IPD_Fund_Code + "'");
                        if (Value == "0")
                        {
                            mysql_comm.ExecuteNonQuery();
                            Log.Write("Sync Fund Information : " + IPD_Fund_Code + "-" + Fund_Name + " Inserted", DateTime.Now.ToString("yyyy-MM-dd"));
                        }
                    }
                }
                mysql_conn.Close();
            }

        }

        protected void Inser_Fund_Nav()
        {
            DS = CallService.Get_NAV_Per_Fund(Start_date, End_date);
            string Sqlconnstring = DB.GetSqlConnection();
            using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
            {
                mysql_conn.Open();
                foreach (DataRow rdr in DS.Rows)
                {
                    string EPF_IPD_Code = ConfigurationManager.AppSettings["IPD_CODE"];

                    string IPD_Fund_Code = rdr[0].ToString();

                    DateTime ForamtedDate = Convert.ToDateTime(rdr[1].ToString());

                    //string day = NAVDATECONVERSION.Substring(0, 2);
                    //string month = NAVDATECONVERSION.Substring(3, 3);
                    //string yr = NAVDATECONVERSION.Substring(NAVDATECONVERSION.Length - 2);
                    //string ForamtedDate = yr + "-" + month + "-" + day;

                    DateTime dt = Convert.ToDateTime(ForamtedDate);
                    string Daily_NAV_Date = dt.ToString("yyyy-MM-dd");
                    string Daily_NAV = rdr[4].ToString();
                    string Daily_Unit_created = rdr[3].ToString();
                    string Daily_Unit_Price = rdr[2].ToString();

                    decimal daily_Unit_Created_EPF = 0.4M;
                    decimal Daily_NAV_EPF = 0.2M;

                    var now = Convert.ToDateTime(ForamtedDate);
                    var startOfMonth = new DateTime(now.Year, now.Month, 1);
                    var DaysInMonth = DateTime.DaysInMonth(now.Year, now.Month);
                    var enddate = new DateTime(now.Year, now.Month, DaysInMonth);

                    string End_date = enddate.ToString("yyyy-MM-dd");

                    string Query = "Insert into utmc_daily_nav_fund("
                        + "EPF_IPD_Code, IPD_Fund_Code, Daily_NAV_Date, Daily_NAV, Report_Date,"
                           + "Daily_Unit_Created, Daily_NAV_EPF, Daily_Unit_Created_EPF, Daily_Unit_Price)"
                             + " values('" + EPF_IPD_Code + "','" + IPD_Fund_Code + "','" + Daily_NAV_Date + "','" + Daily_NAV + "','" + End_date + "','"
                             + Daily_Unit_created + "', " + " '" + Daily_NAV_EPF + "','" + daily_Unit_Created_EPF + "','" + Daily_Unit_Price + "');";



                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                    {

                        string Value = DB.Find_Value_exists("select * from utmc_daily_nav_fund where Daily_NAV_Date='" + Daily_NAV_Date + "' and IPD_Fund_Code='" + IPD_Fund_Code + "'");
                        if (Value == "0")
                        {
                            mysql_comm.ExecuteNonQuery();
                            Log.Write("UTMC_018 : " + IPD_Fund_Code + Daily_NAV_Date + "-" + Daily_NAV_EPF + " Inserted", End_date);
                        }
                    }
                }

                mysql_conn.Close();
            }
        }

        protected void Insert_FundInfo_Details()
        {
            DS = CallService.Get_FundInfo_Details();
            string Sqlconnstring = DB.GetSqlConnection();
            using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
            {
                mysql_conn.Open();
                foreach (DataRow rdr in DS.Rows)
                {
                    string EPF_IPD_Code = ConfigurationManager.AppSettings["IPD_CODE"];

                    string IPD_Fund_Code = rdr[0].ToString();
                    string IPD_Fund_Code_ID = DB.Read_Single_MYSQL_Column_Value("select id from utmc_fund_information where IPD_Fund_Code = '" + IPD_Fund_Code + "'");
                    ///////////
                    string NAVDATECONVERSION = rdr[8].ToString();
                    NAVDATECONVERSION = NAVDATECONVERSION.Substring(0, NAVDATECONVERSION.Length - 3);
                    string LanchPrice = "0.00";     //
                    string PricingBasis = "0";       //
                    string Latest_NAV_Price = DB.Read_Single_MYSQL_Column_Value("select Daily_Unit_Price from utmc_daily_nav_fund where Daily_NAV_Date in (select max(Daily_NAV_Date) from utmc_daily_nav_fund where IPD_Fund_Code='" + IPD_Fund_Code + "') and IPD_Fund_Code='" + IPD_Fund_Code + "'");
                    string Histrocial_Income_Distrubution = "0";        //
                    string ApprovedByEPF = rdr[7].ToString();
                    if (ApprovedByEPF == "Y")
                        ApprovedByEPF = "1";
                    else ApprovedByEPF = "0";
                    string SHARIAH_COMPLIANT = "0";
                    string RiskRating = "0";                             //
                    string FundSize = rdr[15].ToString();
                    string Minimum_Intial_Amount = rdr[12].ToString();
                    string Minimum_Subsequent_Investment = rdr[11].ToString();
                    string Minimum_RSP_Investment = "0";                           //
                    string Minimum_Redemption_Amount = rdr[3].ToString();
                    string Minimum_Holding = rdr[6].ToString();
                    string Cooling_off_Period = rdr[13].ToString();
                    string Distribution_Policy = "0";                            //
                    string BUY_PROCESS_TIME = "0";
                    string RED_PROCESS_TIME = "0";


                    string Query = "INSERT INTO utmc_fund_details "
                                    + " (`utmc_fund_information_id`,`LAUNCH_DATE`,`LAUNCH_PRICE`,`PRICING_BASIS`,`LATEST_NAV_PRICE`,`HISTORICAL_INCOME_DISTRIBUTION`, "
                                    + "`IS_EPF_APPROVED`,`SHARIAH_COMPLIANT`,`RISK_RATING`,`FUND_SIZE_RM`,`MIN_INITIAL_INVEST`,`MIN_SUBSEQUENT_INVEST`,`MIN_RSP_INVEST`,`MIN_RED_AMT`,`MIN_HOLDING`,"
                                    + "`COLLING_OFF_PERIOD`,`DISTRIBUTION_POLICY`,`BUY_PROCESS_TIME`,`RED_PROCESS_TIME`)VALUES('" + IPD_Fund_Code_ID + "', '" + NAVDATECONVERSION + "', '" + LanchPrice + "',"
                                    + " '" + PricingBasis + "', '" + Latest_NAV_Price + "', '" + Histrocial_Income_Distrubution + "', '" + ApprovedByEPF + "','" + SHARIAH_COMPLIANT + "', '" + RiskRating + "',"
                                    + " '" + FundSize + "', '" + Minimum_Intial_Amount + "', '" + Minimum_Subsequent_Investment + "', '" + Minimum_RSP_Investment + "', '" + Minimum_Redemption_Amount + "', '" + Minimum_Holding + "',"
                                    + " '" + Cooling_off_Period + "', '" + Distribution_Policy + "', '" + BUY_PROCESS_TIME + "', '" + RED_PROCESS_TIME + "')";


                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                    {


                        if (IPD_Fund_Code_ID != "")
                        {
                            mysql_comm.ExecuteNonQuery();
                            Log.Write("Insert_FundInfo_Details : " + IPD_Fund_Code + "-" + NAVDATECONVERSION + " Inserted", "");
                        }
                    }
                }

                mysql_conn.Close();
            }
        }


        public void Insert_FundDetails_emis()
        {

            //var now = Convert.ToDateTime(EndDate.Text);
            //var startOfMonth = new DateTime(now.Year, now.Month, 1);
            //var DaysInMonth = DateTime.DaysInMonth(now.Year, now.Month);
            //var enddate = new DateTime(now.Year, now.Month, DaysInMonth);

            //string End_date = enddate.ToString("yyyy-MM-dd");


            DS = CallService.Get_FundInfo_Details_emis();
            string Sqlconnstring = DB.GetSqlConnection();
            using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
            {
                mysql_conn.Open();
                foreach (DataRow rdr in DS.Rows)
                {
                    string EPF_IPD_Code = ConfigurationManager.AppSettings["IPD_CODE"];
                    string IPD_Fund_Code = rdr[2].ToString();
                    string EPF_FundCode = DB.Read_Single_MYSQL_Column_Value("select Fund_Code from utmc_fund_information where IPD_Fund_Code = '" + IPD_Fund_Code + "'");
                    string fund_name = rdr[0].ToString();
                    string shariah_compliant = DB.Read_Single_MYSQL_Column_Value("select Conventional from utmc_fund_information where IPD_Fund_Code = '" + IPD_Fund_Code + "'");
                    string epf_fund_status = DB.Read_Single_MYSQL_Column_Value("select Status from utmc_fund_information where IPD_Fund_Code = '" + IPD_Fund_Code + "'");
                    // string fund_Size = DB.Read_Single_MYSQL_Column_Value("select Daily_NAV from utmc_daily_nav_fund where Daily_NAV_Date in (select max(Daily_NAV_Date) from utmc_daily_nav_fund where IPD_Fund_Code='" + IPD_Fund_Code + "') and IPD_Fund_Code='" + IPD_Fund_Code + "'");
                    string fund_Size = DB.Read_Single_MYSQL_Column_Value("select Daily_NAV from utmc_daily_nav_fund where Daily_NAV_Date='" + End_date + "' and IPD_Fund_Code='" + IPD_Fund_Code + "'");


                    string fund_size_date = End_date;
                    string fund_size_currency = "MYR";
                    string fund_currency_code = "MYR";
                    //   string total_units_in_circulation = DB.Read_Single_MYSQL_Column_Value("select Daily_Unit_Created from utmc_daily_nav_fund where Daily_NAV_Date in (select max(Daily_NAV_Date) from utmc_daily_nav_fund where IPD_Fund_Code='" + IPD_Fund_Code + "') and IPD_Fund_Code='" + IPD_Fund_Code + "'");
                    string total_units_in_circulation = DB.Read_Single_MYSQL_Column_Value("select Daily_Unit_Created from utmc_daily_nav_fund where Daily_NAV_Date='" + End_date + "' and IPD_Fund_Code='" + IPD_Fund_Code + "'");

                    string total_units_in_circulation_date = End_date;
                    string Minimum_Intial_Amount = rdr[3].ToString();
                    string min_subsequent_invest = rdr[4].ToString();
                    string min_redemption = rdr[5].ToString();
                    string min_holding = rdr[6].ToString();


                    string Query = "INSERT INTO funds_info(epf_ipd_code, epf_fund_code, ipd_fund_code, fund_name, shariah_compliant, "
                               + " epf_fund_status, fund_size, fund_size_date, fund_size_currency, fund_currency_code, total_units_in_circulation, "
                              + "  total_units_in_circulation_date, min_initial_invest, min_subsequent_invest, min_redemption,min_holding) "
                              + "  VALUES('" + EPF_IPD_Code + "','" + EPF_FundCode + "','" + IPD_Fund_Code + "','" + fund_name + "','" + shariah_compliant + "',"
                             + " '" + epf_fund_status + "','" + fund_Size + "','" + fund_size_date + "','" + fund_size_currency + "', "
                               + " '" + fund_currency_code + "','" + total_units_in_circulation + "','" + total_units_in_circulation_date + "','" + Minimum_Intial_Amount + "', "
                             + " '" + min_subsequent_invest + "','" + min_redemption + "','" + min_holding + "')";


                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                    {
                        string Value = DB.Find_Value_exists("select * from funds_info where ipd_fund_code='" + IPD_Fund_Code + "'");
                        if (Value == "0")
                        {
                            mysql_comm.ExecuteNonQuery();
                            Log.Write("Insert_FundInfo_Details : " + IPD_Fund_Code + "-" + EPF_IPD_Code + " Inserted", "");
                        }
                    }

                    DB.SQL_ExcuteQuery(" UPDATE funds_info set total_units_in_circulation_date='"+End_date+"',fund_size_date='"+End_date+"', fund_size ='" + fund_Size + "', total_units_in_circulation ='" + total_units_in_circulation + "' where ipd_fund_code ='" + IPD_Fund_Code + "'");
                }

                mysql_conn.Close();
            }
        }
    

        protected void Insert_CSV_files()
        {
            List<ELC_REG> elcRegistrations = ReadFromCSV();
        }

        private List<ELC_REG> ReadFromCSV()
        {

            OpenFileDialog openfiledialog1 = new OpenFileDialog();
            openfiledialog1.ShowDialog();
            openfiledialog1.Filter = "allfiles|*.xls";
            // txtfilepath.Text = openfiledialog1.FileName;




            List<ELC_REG> elcRegistrations = new List<ELC_REG>();
            using (StreamReader inputStreamReader = new StreamReader(""))
            {
                try
                {
                    var line = inputStreamReader.ReadLine();
                    var verifyline = line.Contains("DATE,NAME,DATE RCVD,EPF NO,IC NO,AMOUNT,AGENT,COLL CENTRE");
                    if (verifyline)
                    {
                        while (!inputStreamReader.EndOfStream)
                        {
                            line = inputStreamReader.ReadLine();
                            var values = line.Split(',');
                        }
                    }
                }

                catch (Exception ex)
                {

                    throw ex;

                }
            }
            return elcRegistrations;
        }

        //report 10
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {

                DS = CallService.Get_Member_Transactions(Start_date, End_date, BirthDateupdate);
                string Sqlconnstring = DB.GetSqlConnection();
                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();
                    foreach (DataRow rdr in DS.Rows)
                    {
                        string EPF_IPD_Code = ConfigurationManager.AppSettings["IPD_CODE"];


                        decimal Cost_RM = 0.00M;
                        string IPD_Fund_Code = rdr[0].ToString();
                        string EPF_No = rdr[1].ToString();
                        string IPD_Member_Acc_No = rdr[2].ToString();
                        DateTime dt = Convert.ToDateTime(rdr[3].ToString());

                        var startDate = new DateTime(dt.Year, dt.Month, 1);
                        var endDate = startDate.AddMonths(1).AddDays(-1);
                        string LastDay = endDate.ToString("yyyy-MM-dd");

                        string Effective_Date = dt.ToString("yyyy-MM-dd");
                        //string Date_Of_Transaction = rdr[7].ToString();                                 
                        string Settlementdate = rdr[13].ToString();
                        if (Settlementdate == "")
                        {
                            Settlementdate = Effective_Date;
                        }
                        else
                        {
                            DateTime dt1 = Convert.ToDateTime(rdr[13].ToString());
                            Settlementdate = dt1.ToString("yyyy-MM-dd");
                        }
                        // DateTime dt1 = Convert.ToDateTime(rdr[13].ToString());
                        // string Settlementdate = dt1.ToString("yyyy-MM-dd");

                        string IPD_Unique_Transaction_ID = rdr[4].ToString();
                        string Transaction_Code = rdr[5].ToString().Trim();
                        string AccountType = rdr[6].ToString().Trim();
                        string Reversed_Transaction_ID = "";
                        decimal AverageCost = Convert.ToDecimal(rdr[14].ToString());

                        string Sort_SEQ = rdr[15].ToString();

                        decimal Units = Math.Abs(Convert.ToDecimal(rdr[7].ToString()));
                        decimal Gross_Amount_RM = Convert.ToDecimal(rdr[8].ToString());
                        decimal Net_Amount_RM = Convert.ToDecimal(rdr[11].ToString());
                        decimal Fees_RM = Convert.ToDecimal(rdr[9].ToString());
                        decimal GST_RM = Convert.ToDecimal(rdr[10].ToString());
                        decimal Proceeds_RM = 0.00M;
                        decimal Realised_Gain_Loss = 0.00M;
                        AverageCost = decimal.Round(AverageCost, 4);
                        char Check_reversed = IPD_Unique_Transaction_ID[IPD_Unique_Transaction_ID.Length - 1];

                        if (Check_reversed == 'X')
                        {
                            if (Transaction_Code == "SA")
                            {

                                if (AccountType == "SW")
                                {
                                    Transaction_Code = "XI";
                                    Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                }
                                else
                                {
                                    Transaction_Code = "XS";
                                    Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                }



                            }
                            else if (Transaction_Code == "RD")
                            {

                                if (AccountType == "SW")
                                {
                                    Transaction_Code = "XO";
                                    Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                }
                                else
                                {
                                    Transaction_Code = "XR";
                                    Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                }
                            }

                            else if (Transaction_Code == "DD")
                            {

                                if (Units == 0)
                                {
                                    Transaction_Code = "XD";
                                    Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                }
                                else
                                {
                                    Transaction_Code = "XV";
                                    Reversed_Transaction_ID = IPD_Unique_Transaction_ID;
                                }

                            }
                        }





                        if ((Transaction_Code == "SA") || (Transaction_Code == "DD"))
                        {

                        }
                        if ((Transaction_Code == "SA") && (AccountType == "SW"))
                        {
                            Transaction_Code = "SI";

                        }
                        if ((Transaction_Code == "RD") && (AccountType == "SW"))
                        {
                            Transaction_Code = "SO";
                            //Gross_Amount_RM = 0.00M;                  // as per request SO
                            Net_Amount_RM = 0.00M;

                        }


                        if ((Transaction_Code == "TR") || (Transaction_Code == "RD") || (Transaction_Code == "SO") || (Transaction_Code == "XR") || (Transaction_Code == "XD") || (Transaction_Code == "XO") || (Transaction_Code == "XV"))
                        {
                            Proceeds_RM = Gross_Amount_RM;
                            // Cost_RM = Units * AverageCost;
                            Realised_Gain_Loss = Proceeds_RM - Cost_RM;
                            // Cost_RM = decimal.Round(Cost_RM, 2);
                            Proceeds_RM = decimal.Round(Proceeds_RM, 2);
                            Realised_Gain_Loss = decimal.Round(Realised_Gain_Loss, 2);


                        }

                        //filter by sign requirement 

                        if ((Transaction_Code == "TR") || (Transaction_Code == "RD") || (Transaction_Code == "SO")
                            || (Transaction_Code == "XS") || (Transaction_Code == "XC") || (Transaction_Code == "XD")
                            || (Transaction_Code == "XV") || (Transaction_Code == "UX") || (Transaction_Code == "XI")
                            || (Transaction_Code == "RO") || (Transaction_Code == "RI") || (Transaction_Code == "HO") || (Transaction_Code == "XO"))
                        {

                            if (!(Net_Amount_RM == 0))
                            {
                                Net_Amount_RM = -Net_Amount_RM;

                            }
                            if (!(Fees_RM == 0))
                            {
                                Fees_RM = -Fees_RM;

                            }
                            if (!(GST_RM == 0))
                            {
                                GST_RM = -GST_RM;

                            }
                            Gross_Amount_RM = -Gross_Amount_RM;

                            if (!(Transaction_Code == "XO"))
                            {
                                Units = -Units;
                            }
                            if ((Transaction_Code == "TR") || (Transaction_Code == "RD") || (Transaction_Code == "SO"))
                            {
                                Proceeds_RM = -Proceeds_RM;
                            }


                        }
                        ////////// hard code  adjustment Start


                        if (Transaction_Code == "SA")
                        {
                            Transaction_Code = "NS";
                            // final  aaa ---
                            Cost_RM = Gross_Amount_RM;
                            // final  aaa ---

                        }
                        if (Transaction_Code == "SI")
                        {
                            // final  aaa ---
                            Cost_RM = Gross_Amount_RM;
                            // final  aaa ---


                        }
                        if (Transaction_Code == "DD")
                        {
                            Transaction_Code = "DV";
                            Realised_Gain_Loss = 0.00M;
                        }




                        if (Transaction_Code == "XD")
                        {
                            Realised_Gain_Loss = 0.00M;
                            // Net_Amount_RM = Gross_Amount_RM;
                            Proceeds_RM = 0.00M;

                        }

                        if (Transaction_Code == "XV")
                        {
                            Realised_Gain_Loss = 0.00M;

                            // Net_Amount_RM = Gross_Amount_RM;
                            Proceeds_RM = 0.00M;
                        }




                        if (Transaction_Code == "RD")
                        {
                            Transaction_Code = "RE";
                            Cost_RM = Get_Redumption_Cost_UTMC10(IPD_Member_Acc_No, IPD_Fund_Code, Sort_SEQ);


                            Realised_Gain_Loss = Math.Abs(Proceeds_RM) - Math.Abs(Cost_RM);

                            // final  aaa ---
                            Gross_Amount_RM = Proceeds_RM;
                            Net_Amount_RM = Proceeds_RM;
                            // final  aaa ---

                            Cost_RM = -Cost_RM;
                        }
                        if (Transaction_Code == "TR")
                        {
                            Transaction_Code = "TO";
                            Cost_RM = Get_Redumption_Cost_UTMC10(IPD_Member_Acc_No, IPD_Fund_Code, Sort_SEQ);
                            Realised_Gain_Loss = Math.Abs(Proceeds_RM) - Math.Abs(Cost_RM);
                            // final  aaa ---
                            Gross_Amount_RM = Proceeds_RM;
                            Net_Amount_RM = Proceeds_RM;
                            // final  aaa ---
                        }


                        if (Transaction_Code == "SO" || Transaction_Code == "XO")
                        {
                            Cost_RM = Get_Redumption_Cost_UTMC10(IPD_Member_Acc_No, IPD_Fund_Code, Sort_SEQ);
                            Realised_Gain_Loss = Math.Abs(Proceeds_RM) - Math.Abs(Cost_RM);
                            // final  aaa ---
                            Gross_Amount_RM = Proceeds_RM;
                            Net_Amount_RM = Proceeds_RM;
                            // final  aaa ---
                            Cost_RM = -Cost_RM;
                        }

                        if (Transaction_Code == "XO")
                        {
                            Cost_RM = -Cost_RM;
                            Realised_Gain_Loss = -Realised_Gain_Loss;
                            //Units = Units;

                        }


                        // FILTERED 0 UNIT DIVIDEND 
                        Boolean bolFILTER = true;
                        if (Transaction_Code == "DV")
                        {
                            if (Units == 0)
                            {
                                bolFILTER = false;
                            }

                        }

                        if (Transaction_Code == "XS")

                        {
                            Cost_RM = Gross_Amount_RM;

                        }


                        if (Transaction_Code == "XI" || Transaction_Code == "XR")
                        {
                            Cost_RM = Get_Redumption_Cost_UTMC10(IPD_Member_Acc_No, IPD_Fund_Code, Sort_SEQ);

                            if (Transaction_Code == "XI")
                            {
                                Cost_RM = Gross_Amount_RM;
                            }

                            if (Transaction_Code == "XR")
                            {
                                Realised_Gain_Loss = -(Proceeds_RM - Cost_RM);
                                Realised_Gain_Loss = decimal.Round(Realised_Gain_Loss, 2);

                            }

                        }

                        if (Transaction_Code == "DV" || Transaction_Code == "XV")
                        {
                            Net_Amount_RM = Gross_Amount_RM;
                        }





                        //  Log.Write(ex.ToString(), End_date);

                        //////////////////////////////////////////////////////////////////////////////////////////// hard code  adjustment end
                        string Query =
                              "INSERT INTO utmc_compositional_transactions "
                              + " (EPF_IPD_Code, IPD_Fund_Code, Member_EPF_No, IPD_Member_Acc_No, Effective_Date, Date_Of_Transaction, Date_Of_Settlement, IPD_Unique_Transaction_ID, Transaction_Code, Reversed_Transaction_ID, Units, "
                              + " Gross_Amount_RM, Net_Amount_RM, Fees_RM, GST_RM, Cost_RM, Proceeds_RM, Realised_Gain_Loss,report_date) VALUES "
                              + "('" + EPF_IPD_Code + "','" + IPD_Fund_Code + "','" + EPF_No + "','" + IPD_Member_Acc_No + "','" + LastDay + "','" + Effective_Date + "','" + Settlementdate + "','" + IPD_Unique_Transaction_ID + "','" + Transaction_Code + "','" + Reversed_Transaction_ID + "','" + Units + "','" + Gross_Amount_RM + "','" + Net_Amount_RM + "','" + Fees_RM + "','" + GST_RM + "','" + Cost_RM + "','" + Proceeds_RM + "','" + Realised_Gain_Loss + "','" + End_date + "')";
                        using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                        {                         
                            if (bolFILTER == true)
                            {
                                string Value = DB.Find_Value_exists("select * from utmc_compositional_transactions where Date_Of_Transaction='" + Effective_Date + "' and IPD_Fund_Code='" + IPD_Fund_Code + "'");
                                if (Value == "0")
                                    mysql_comm.ExecuteNonQuery();
                                Log.Write("UTMC_010 : " + IPD_Fund_Code + IPD_Fund_Code + IPD_Member_Acc_No + "Inserted", End_date);
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                Log.Write(ex.ToString(), End_date);
            }
        }
        //report 10
        protected decimal Get_Redumption_Cost_UTMC10(string Holder_No, string Fund_ID, string Sort_Seq)
        {
            try
            {

                decimal Actual_transfer_value = 0.0000M;
                decimal Book_value = 0.0000M;
                decimal Unit_Holding_value = 0.0000M;
                string Trans_Type = "";
                decimal Total_Redemption_cost = 0.0000M;
                decimal UnitCostRD = 0.0000M;
                decimal UnitCostRDfinal = 0.0000M;
                int Count = 0; string[] numb;

                var dataTable = new System.Data.DataTable();
                dataTable = CallService.Get_RedumptionCost(Holder_No, Fund_ID, End_date);

                numb = new string[dataTable.Rows.Count];

                foreach (System.Data.DataRow row in dataTable.Rows)
                {
                    numb[Count++] = row[0].ToString();
                }
                Array.Resize(ref numb, Count);

                Count = 0;
                string Trans_String;

                foreach (System.Data.DataRow row in dataTable.Rows)
                {
                    try
                    {

                        Trans_String = row[0].ToString();
                        bool TTT = false;


                        // check reversal
                        for (int i = Count; i < numb.Length; i++)
                        {
                            if (Trans_String + "X" == numb[i].ToString())
                            {
                                // rdr.NextResult();
                                TTT = true;
                                break;

                            }

                            char Check_reversed = Trans_String[Trans_String.Length - 1];

                            if (Check_reversed == 'X')
                            {
                                TTT = true;

                                Unit_Holding_value = Convert.ToDecimal(row[8].ToString());
                                Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);

                                UnitCostRD = Convert.ToDecimal(row[5].ToString());
                                UnitCostRD = decimal.Round(UnitCostRD, 4);

                                break;
                            }

                        }

                        Count++;

                        if (TTT == false)
                        {


                            Trans_Type = row[1].ToString().Trim();

                            if (Trans_Type == "SA")
                            {
                                string Trans_no = row[0].ToString();
                                char Check_reversed = Trans_no[Trans_no.Length - 1];

                                if (Check_reversed == 'X')
                                {
                                    Trans_no = Trans_no.TrimEnd(Trans_no[Trans_no.Length - 1]);
                                    Actual_transfer_value -= Convert.ToDecimal(row[6].ToString());
                                    Book_value -= Convert.ToDecimal(row[7].ToString());
                                    Book_value = decimal.Round(Book_value, 2);
                                    Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);

                                }
                                else
                                {
                                    Actual_transfer_value += Convert.ToDecimal(row[6].ToString());
                                    Book_value += Convert.ToDecimal(row[7].ToString());
                                    Book_value = decimal.Round(Book_value, 2);
                                    Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);

                                }

                            }
                            if ((Trans_Type == "DD") || (Trans_Type == "BI") || (Trans_Type == "SA") || (Trans_Type == "UC"))
                            {
                                Unit_Holding_value = Convert.ToDecimal(row[8].ToString());
                                Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);

                            }
                            if (Trans_Type == "RD" || Trans_Type == "TR")
                            {



                                Total_Redemption_cost = Convert.ToDecimal(row[8].ToString());
                                Total_Redemption_cost = decimal.Round(Total_Redemption_cost, 4);

                                UnitCostRD = Convert.ToDecimal(row[5].ToString());
                                UnitCostRD = decimal.Round(UnitCostRD, 4);


                                /* FINAL AAAA OLD COST
                                UnitCostRDfinal = (Book_value / Convert.ToDecimal(Unit_Holding_value)  //  Check RDCost                                                                       * Total_Redemption_cost);
                                                    * UnitCostRD);
                                /* FINAL AAAA OLD COST */

                                UnitCostRDfinal = (Actual_transfer_value / Convert.ToDecimal(Unit_Holding_value)  //  Check RDCost                                                                       * Total_Redemption_cost);
                                                    * UnitCostRD);


                                UnitCostRDfinal = decimal.Round(UnitCostRDfinal, 2);


                                if (Convert.ToDecimal(Total_Redemption_cost) > 0)
                                {



                                    Actual_transfer_value = (Actual_transfer_value / Convert.ToDecimal(Unit_Holding_value)
                                                               * Total_Redemption_cost);
                                    Book_value = (Book_value / Convert.ToDecimal(Unit_Holding_value)
                                                              * Total_Redemption_cost);



                                    Book_value = decimal.Round(Book_value, 2);
                                    Actual_transfer_value = decimal.Round(Actual_transfer_value, 2);


                                }
                                else
                                {
                                    Actual_transfer_value = 0.0000M;
                                    Book_value = 0.0000M;
                                    Unit_Holding_value = 0.0000M;
                                }

                                // if (Trans_Type == "TR")
                                // {
                                //   UnitCostRDfinal = 0.00M;
                                // }

                                Unit_Holding_value = Convert.ToDecimal(row[8].ToString());
                                Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);

                                if (Sort_Seq == row[10].ToString())
                                {
                                    break;
                                }

                            }

                        }

                    }
                    catch (Exception ex)
                    {
                        Log.Write(ex.ToString(), End_date);
                    }

                }

                return UnitCostRDfinal;
            }

            catch (Exception ex)
            {
                Log.Write(ex.ToString(), End_date);
                return 0.00M;

            }
        }


        #region start
        //report 11
        private void button4_Click(object sender, EventArgs e)
        {

            Generate_Report15();
        }

        public void Generate_Report_11()
        { 

            DateTime dateTime = Convert.ToDateTime(Start_date);

            dateTime = dateTime.AddDays(-1);
            string PrevMonthDate = dateTime.ToString("yyyy-MM-dd");

            string ConnectionString = GetSqlConnection();
            try
            {
                using (MySqlConnection conn = new MySqlConnection(ConnectionString))
                {
                    conn.Open();


                    string mysql = "select IPD_Fund_Code, Member_EPF_No , IPD_Member_Acc_No,max(Report_Date) FROM utmc_member_investment where "
                                      + " (Report_Date='" + End_date + "' "
                                      + " or  Report_Date ='" + PrevMonthDate + "') AND Units > 0"
                                      + " group by IPD_Member_Acc_No,IPD_Fund_Code order by IPD_Member_Acc_No";

                    using (MySqlCommand comm = new MySqlCommand(mysql, conn))
                    {
                        using (MySqlDataReader rdr = comm.ExecuteReader())
                        {
                            using (MySqlConnection mysql_conn = new MySqlConnection(ConnectionString))
                            {
                                string EPF_IPD_Code = ConfigurationManager.AppSettings["IPD_CODE"];
                                mysql_conn.Open();
                                while (rdr.Read())
                                {
                                    string IPD_Fund_Code = rdr[0].ToString();
                                    string EPF_No = rdr[1].ToString();
                                    string IPD_Member_Acc_No = rdr[2].ToString();
                                    // Next
                                    string Next__Opening_Balance_Units = "0.00";          //1
                                    string Next_Opening_Balance_Cost_RM = "0.00";         //2                        

                                    // Current
                                    decimal Curr_Net_Cumulative_Closing_Balance_Units = 0.00M;    //1
                                    decimal Curr_Net_Cumulative_Closing_Balance_Cost_RM = 0.00M;   //2                                  
                                    decimal Curr_Market_Price_NAV = 0.00M;                     //
                                    decimal Curr_Unrealised_Gain_Loss_RM = 0.00M;              //


                                    /* gordon */

                                    /* gordon */

                                    string Query =
                                    "INSERT INTO utmc_compositional_investment(EPF_IPD_Code, IPD_Fund_Code, Member_EPF_No, IPD_Member_Acc_No, Effective_Date, Opening_Balance_Units, Opening_Balance_Cost_RM, Opening_Balance_Date, Net_Cumulative_Closing_Balance_Units,Net_Cumulative_Closing_Balance_Cost_RM,Net_Cumulative_Closing_Balance_Date,Market_Price_NAV,Market_Price_Effective_Date,Unrealised_Gain_Loss_RM,Report_Date,Report_Key)"
                                   + " VALUES ('" + EPF_IPD_Code + "','" + IPD_Fund_Code + "','" + EPF_No + "','" + IPD_Member_Acc_No + "','" + End_date + "','" + Next__Opening_Balance_Units + "','" + Next_Opening_Balance_Cost_RM + "','" + Start_date + "','" + Curr_Net_Cumulative_Closing_Balance_Units + "','" + Curr_Net_Cumulative_Closing_Balance_Cost_RM + "','" + End_date + "','" + Curr_Market_Price_NAV + "','" + End_date + "','" + Curr_Unrealised_Gain_Loss_RM + "','" + End_date + "','" + End_date + "');";


                                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                    {
                                        try
                                        {
                                            string Value = DB.Find_Value_exists("select * from utmc_compositional_investment where Member_EPF_No='" + EPF_No + "' and IPD_Fund_Code='" + IPD_Fund_Code + "' and Report_Date='" + End_date + "'");
                                            if (Value == "0")
                                             mysql_comm.ExecuteNonQuery();
                                            Log.Write("UTMC_011 : " + IPD_Fund_Code + " " + IPD_Member_Acc_No + "Inserted", End_date);

                                        }
                                        catch
                                        {
                                            continue;
                                        }
                                    }

                                }
                                rdr.Close();
                                mysql_conn.Close();
                            }
                        }
                        conn.Close();
                    }
                }

                System.Threading.Thread.Sleep(2000);
                ReadHolderNoforutmc11();
            }

            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        protected void ReadHolderNoforutmc11()
        {

            string Sqlconnstring = GetSqlConnection();
            try
            {
                string Mysql = "SELECT IPD_Member_Acc_No,IPD_Fund_Code  FROM utmc_compositional_investment where Report_Date='" + End_date + "'";
                string ReportDt = End_date;

                DateTime dateTime = Convert.ToDateTime(Start_date);
                dateTime = dateTime.AddDays(-1);
                string OpeningDate = dateTime.ToString("yyyy-MM-dd");

                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();

                    using (MySqlCommand comm = new MySqlCommand(Mysql, mysql_conn))
                    {
                        using (MySqlDataReader rdr = comm.ExecuteReader())
                        {


                            var dataTable = new System.Data.DataTable();
                            dataTable.Load(rdr);

                            rdr.Close();
                            mysql_conn.Close();

                            foreach (System.Data.DataRow row in dataTable.Rows)
                            {
                                string EPF_No = row[0].ToString();
                                string IPD_fund_Code = row[1].ToString();
                                UpdateBalance(EPF_No, IPD_fund_Code, OpeningDate, false);
                                UpdateBalance(EPF_No, IPD_fund_Code, ReportDt, true);
                            }


                            //while (rdr.Read())
                            //{
                            //    string EPF_No = rdr[0].ToString();
                            //    string IPD_fund_Code = rdr[1].ToString();
                            //    UpdateBalance(EPF_No, IPD_fund_Code, OpeningDate, false);
                            //    UpdateBalance(EPF_No, IPD_fund_Code, ReportDt, true);
                            //}
                            //rdr.Close();
                        }

                        // mysql_conn.Close();
                    }
                }
                Log.Write("UTMC 11 for " + Start_date + "to" + End_date + "import from oracle", End_date);
            }
            catch (Exception ex)
            {
                ex.ToString();
                Log.Write("UTMC 11 for " + Start_date + "to" + End_date + "import from oracle - failed", End_date);

            }

        }

        protected void UpdateBalance(string HolderNo, string IPD_fund_Code, string ReportDt, bool IsClosingUpdate)

        {

            string Sqlconnstring = GetSqlConnection();
            try
            {
                using (MySqlConnection conn = new MySqlConnection(Sqlconnstring))
                {
                    conn.Open();
                    string mysql = "select Actual_Transferred_From_EPF_RM,Units,Market_Value,Report_Date from utmc_member_investment where Report_Date='" + ReportDt + "' AND  IPD_Member_Acc_No='" + HolderNo + "' and IPD_Fund_Code='" + IPD_fund_Code + "' ";

                    using (MySqlCommand comm = new MySqlCommand(mysql, conn))
                    {
                        using (MySqlDataReader rdr = comm.ExecuteReader())
                        {
                            using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                            {
                                mysql_conn.Open();
                                string Query = "";
                                bool det = false;

                                if (!IsClosingUpdate)
                                {
                                    det = true;
                                }

                                while (rdr.Read())
                                {
                                    det = true;

                                    if (IsClosingUpdate)
                                    {


                                        string Balance = rdr[0].ToString();
                                        string Net_Cumulative_Closing_Balance_Units = rdr[1].ToString();
                                        string MarketPrice = rdr[2].ToString();
                                        string Gain = (Convert.ToDecimal(MarketPrice) - Convert.ToDecimal(Balance)).ToString();

                                        /* gordon */
                                        string MarketPricePU = decimal.Round((Convert.ToDecimal(MarketPrice) / Convert.ToDecimal(Net_Cumulative_Closing_Balance_Units)), 4).ToString();

                                        Query = "UPDATE utmc_compositional_investment SET Net_Cumulative_Closing_Balance_Units='" + Net_Cumulative_Closing_Balance_Units + "',Net_Cumulative_Closing_Balance_Cost_RM='" + Balance + "', Market_Price_NAV='" + MarketPricePU + "', Unrealised_Gain_Loss_RM='" + Gain + "' where IPD_Member_Acc_No='" + HolderNo + "' AND IPD_Fund_Code= '" + IPD_fund_Code + "' and Report_Date='" + End_date + "'";
                                        /* gordon */

                                    }
                                    else
                                    {
                                        string OpeningBalance = rdr[0].ToString();
                                        string OpeningBalanceUnit = rdr[1].ToString();

                                        Query = "UPDATE utmc_compositional_investment SET Opening_Balance_Units='" + OpeningBalanceUnit + "',Opening_Balance_Cost_RM='" + OpeningBalance + "'  where IPD_Member_Acc_No='" + HolderNo + "' AND IPD_Fund_Code= '" + IPD_fund_Code + "' and Report_Date='" + End_date + "'";

                                    }

                                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                                    {
                                        try
                                        {
                                            mysql_comm.ExecuteNonQuery();
                                        }
                                        catch
                                        {
                                            continue;
                                        }
                                    }



                                }

                                if (!det)
                                {
                                    // UpdateReprot15_RD(HolderNo, IPD_fund_Code, ReportDt);


                                }
                                rdr.Close();
                                mysql_conn.Close();
                            }
                        }
                        conn.Close();
                    }
                }
            }


            catch (Exception ex)
            {
                ex.ToString();
                Log.Write("UTMC 11 - UpdateBalance " + HolderNo + " import from oracle - failed", End_date);

            }

        }
        #endregion


        public string GetSqlConnection()
        {


            string conStr = DB.GetSqlConnection();
            return conStr;
        }

        public string GetOracleConnection()
        {

            string conStr = DB.GetOracleConnection();
            return conStr;

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Start_date = StartDate.Text;
            End_date = EndDate.Text;
            DateSelection.Text = "Import record between " + Start_date + " to " + End_date + "";


            DateTime date = Convert.ToDateTime(End_date);
            var nextMonth = new DateTime(date.Year, date.Month, 1).AddMonths(1);

            string[] datePartsDOB = Start_date.Split('/');

            int upper = datePartsDOB.GetUpperBound(0);
            if (upper == 2)
            {
                int tmpYear = Convert.ToInt32(datePartsDOB[0]) - 55;
                int tmpMonth = Convert.ToInt32(datePartsDOB[1]) - 1;
                if (tmpMonth == 0)
                {
                    tmpMonth = 12;
                    tmpYear = tmpYear - 1;
                }
                if (tmpMonth == 13)
                {
                    tmpMonth = 1;
                    tmpYear = tmpYear + 1;
                }
                int tmpDay = 1;

                DateTime dtLastTemp = new
                    DateTime(tmpYear, tmpMonth, tmpDay);
                BirthDateupdate = GetLastDayOfMonth(dtLastTemp).ToString("yyyy-MM-dd");

            }
            if (Convert.ToInt32(BirthDateupdate.Replace("-", "")) < 19610831)
            {
                BirthDateupdate = "1961-08-31";
            }

           
            TransDateUpdate = nextMonth.ToString("yyyy-MM-dd");

            // update for diotp and e-MIS


            DateTime datestart1 = Convert.ToDateTime(Start_date);
            Start_date = datestart1.AddDays(-1).ToString(("yyyy-MM-dd"));

            DateTime dateend = Convert.ToDateTime(End_date);
            End_date = dateend.AddDays(-1).ToString(("yyyy-MM-dd"));

            End_date = FindHoliday(End_date);
            Start_date = End_date;

            TransDateUpdate = Convert.ToDateTime(Start_date).AddDays(1).ToString("yyyy-MM-dd");


        }

        protected string FindHoliday(string End_date)
        {

            for (int i = 0; i < 31; i++)
            {
                DateTime dt = Convert.ToDateTime(End_date);
                string FindDay = dt.DayOfWeek.ToString().Trim();
                if ((FindDay == "Saturday") || (FindDay == "Sunday"))
                {
                    DateTime Daily_NAV_Date1 = dt.AddDays(-1);
                    FindDay = dt.DayOfWeek.ToString().Trim();
                    End_date = Daily_NAV_Date1.ToString("yyyy-MM-dd");
                }
                else
                {
                    // check holiday     
                    using (MySqlConnection conn2 = new MySqlConnection(GetSqlConnection()))
                    {
                        conn2.Open();
                        string Query = "SELECT * FROM eppa_wbr_new.holiday WHERE datediff('" + End_date + "', holiday_date)= 0";
                        using (MySqlCommand comm1 = new MySqlCommand(Query, conn2))
                        {
                            using (MySqlDataReader rdr1 = comm1.ExecuteReader())
                            {
                                if (rdr1.HasRows)
                                {
                                    DateTime Daily_NAV_Date1 = dt.AddDays(-1);
                                    FindDay = dt.DayOfWeek.ToString().Trim();
                                    End_date = Daily_NAV_Date1.ToString("yyyy-MM-dd");
                                }
                                else
                                {
                                    End_date = dt.ToString("yyyy-MM-dd");
                                    break;
                                }
                                conn2.Close();
                            }
                        }
                    }
                }
            }
            return End_date;
        }



        private DateTime GetLastDayOfMonth(DateTime dtDate)
        {

            DateTime dtTo = dtDate;
            dtTo = dtTo.AddMonths(1);
            dtTo = dtTo.AddDays(-(dtTo.Day));
            return dtTo;
        }


        //report 15
        private void button5_Click(object sender, EventArgs e)
        {

            string IPD_Fund_Code = "";
            string IPD_Member_Acc_No = "";

            try
            {

                string Sqlconnstring = GetSqlConnection();


                string NAV_PricePU_Dt = End_date;
                NAV_PricePU_Dt = NAV_PricePU_Dt.Replace("/", "-");
                BirthDateupdate = BirthDateupdate.Replace("/", "-");


                DS = CallService.Get_Member_Invesment(NAV_PricePU_Dt, TransDateUpdate, BirthDateupdate);


                string EPF_IPD_Code = ConfigurationManager.AppSettings["IPD_CODE"];


                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();

                    foreach (DataRow rdr in DS.Rows)
                    {
                        string Member_EPF_No = rdr[0].ToString();

                        IPD_Fund_Code = rdr[1].ToString();
                        IPD_Member_Acc_No = rdr[2].ToString();
                        string Actual_Transferred_From_EPF_RM = rdr[4].ToString();
                        string Units = rdr[6].ToString();
                        string Book_Value = rdr[4].ToString();
                        string Market_Value = rdr[11].ToString();


                        // adjustment 2016 09

                        if (End_date == "2016/07/31")
                        {
                            if (IPD_Fund_Code == "06")
                            {

                                decimal Market_Value_TT = 0.00M;
                                decimal Units_TT = Convert.ToDecimal(Units.ToString());
                                Market_Value_TT = (Units_TT * Convert.ToDecimal("0.2021".ToString()));
                                Market_Value_TT = decimal.Round(Market_Value_TT, 4);
                                Market_Value = Convert.ToString(Market_Value_TT.ToString());

                            }
                        }

                        // adjustment 2016 09

                        DateTime dt = Convert.ToDateTime(rdr[5].ToString());
                        string TransDate = dt.ToString("yyyy-MM-dd");

                        DateTime now = Convert.ToDateTime(rdr[5].ToString());
                        var startDate = new DateTime(now.Year, now.Month, 1);
                        var endDate = startDate.AddMonths(1).AddDays(-1);
                        string Effective_Date = End_date;

                        string Query = "Insert into utmc_member_investment(Member_EPF_No,EPF_IPD_Code,IPD_Fund_Code,IPD_Member_Acc_No,Actual_Transferred_From_EPF_RM,Units,Book_Value,Market_Value,Effective_Date,Report_Date) "
                       + " values('" + Member_EPF_No + "','" + EPF_IPD_Code + "','" + IPD_Fund_Code + "', '" + IPD_Member_Acc_No + "','" + Actual_Transferred_From_EPF_RM + "','" + Units + "','" + Book_Value + "','" + Market_Value + "','" + End_date + "','" + End_date + "')";

                        using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                        {

                            string Value = DB.Find_Value_exists("select * from utmc_member_investment where Member_EPF_No='" + Member_EPF_No + "' and IPD_Fund_Code='" + IPD_Fund_Code + "' and Market_Value='" + Market_Value + "'");
                            if (Value == "0")
                                mysql_comm.ExecuteNonQuery();
                            Log.Write("UTMC_015 : " + IPD_Fund_Code + IPD_Fund_Code + " - " + IPD_Member_Acc_No + "Inserted", End_date);
                        }
                    }

                    mysql_conn.Close();
                }
                System.Threading.Thread.Sleep(2000);

                ReadHolderNoforutmc15();
            }

            catch (Exception ex)
            {
                Log.Write("UTMC_015 : " + IPD_Fund_Code +" - "+ IPD_Member_Acc_No + " - Insert Failed", End_date);

                ex.ToString();
            }
        }
     
        private void Generate_Report15()
        {

            string IPD_Fund_Code = "";
            string IPD_Member_Acc_No = "";

            try
            {

                string Sqlconnstring = GetSqlConnection();


                string NAV_PricePU_Dt = End_date;
                NAV_PricePU_Dt = NAV_PricePU_Dt.Replace("/", "-");
                BirthDateupdate = BirthDateupdate.Replace("/", "-");


                DS = CallService.Get_Member_Invesment(NAV_PricePU_Dt, TransDateUpdate, BirthDateupdate);


                string EPF_IPD_Code = ConfigurationManager.AppSettings["IPD_CODE"];


                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();

                    foreach (DataRow rdr in DS.Rows)
                    {
                        string Member_EPF_No = rdr[0].ToString();

                        IPD_Fund_Code = rdr[1].ToString();
                        IPD_Member_Acc_No = rdr[2].ToString();
                        string Actual_Transferred_From_EPF_RM = rdr[4].ToString();
                        string Units = rdr[6].ToString();
                        string Book_Value = rdr[4].ToString();
                        string Market_Value = rdr[11].ToString();
                        string CurrentUnitValue = rdr[6].ToString();

                        // adjustment 2016 09

                        if (End_date == "2016/07/31")
                        {
                            if (IPD_Fund_Code == "06")
                            {

                                decimal Market_Value_TT = 0.00M;
                                decimal Units_TT = Convert.ToDecimal(Units.ToString());
                                Market_Value_TT = (Units_TT * Convert.ToDecimal("0.2021".ToString()));
                                Market_Value_TT = decimal.Round(Market_Value_TT, 4);
                                Market_Value = Convert.ToString(Market_Value_TT.ToString());

                            }
                        }

                        // adjustment 2016 09

                        DateTime dt = Convert.ToDateTime(rdr[5].ToString());
                        string TransDate = dt.ToString("yyyy-MM-dd");

                        DateTime now = Convert.ToDateTime(rdr[5].ToString());
                        var startDate = new DateTime(now.Year, now.Month, 1);
                        var endDate = startDate.AddMonths(1).AddDays(-1);
                        string Effective_Date = End_date;

                        string Query = "Insert into utmc_member_investment(Member_EPF_No,EPF_IPD_Code,IPD_Fund_Code,IPD_Member_Acc_No,Actual_Transferred_From_EPF_RM,Units,Book_Value,Market_Value,Effective_Date,Report_Date,Current_Unit_Holding_Value) "
                       + " values('" + Member_EPF_No + "','" + EPF_IPD_Code + "','" + IPD_Fund_Code + "', '" + IPD_Member_Acc_No + "','" + Actual_Transferred_From_EPF_RM + "','" + Units + "','" + Book_Value + "','" + Market_Value + "','" + End_date + "','" + End_date + "','"+CurrentUnitValue+"')";

                        using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                        {

                            string Value = DB.Find_Value_exists("select * from utmc_member_investment where Effective_Date='" + End_date + "' and IPD_Fund_Code='" + IPD_Fund_Code + "' AND IPD_Member_Acc_No='"+ IPD_Member_Acc_No + "'");
                            if (Value == "0")
                                mysql_comm.ExecuteNonQuery();
                            Log.Write("UTMC_015 : " + IPD_Fund_Code + IPD_Fund_Code + IPD_Member_Acc_No + "Inserted", End_date);

                            //System.Threading.Thread.Sleep(20);
                        }
                    }

                    mysql_conn.Close();
                }
                System.Threading.Thread.Sleep(2000);

                ReadHolderNoforutmc15();
            }

            catch (Exception ex)
            {
                Log.Write("UTMC_015 : " + IPD_Fund_Code + IPD_Member_Acc_No + " - Insert Failed " +ex.ToString(), End_date);

                ex.ToString();
            }       
    }

        protected void ReadHolderNoforutmc15()
        {

            string Sqlconnstring = GetSqlConnection();
            try
            {
                string Mysql = "SELECT IPD_Member_Acc_No,IPD_Fund_Code FROM utmc_member_investment where Report_date=' " + End_date + "'";  //
                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();




                    using (MySqlCommand comm = new MySqlCommand(Mysql, mysql_conn))
                    {
                        using (MySqlDataReader rdr = comm.ExecuteReader())
                        {



                            var dataTable = new System.Data.DataTable();
                            dataTable.Load(rdr);

                            rdr.Close();
                            mysql_conn.Close();

                            foreach (System.Data.DataRow row in dataTable.Rows)
                            {
                                string EPF_No = row[0].ToString();
                                string IPD_fund_Code = row[1].ToString();
                                Get_Redumptiondate(EPF_No, IPD_fund_Code);

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString() + "---Get Final Read Holder No From utmc_member_investment failed--- timeout", End_date);
            }

        }

        protected void Get_Redumptiondate(string Holder_No, string Fund_ID, bool IsInsert = false)
        {

            string Sqlconnstring = GetOracleConnection();
            try
            {

                decimal Auctual_transfer_value = 0.0000M;
                decimal Book_value = 0.0000M;
                decimal Unit_Holding_value = 0.0000M;
                string Trans_Type = "";
                decimal Total_Redumption_cost = 0.0000M;
                decimal UnitCostRD = 0.0000M;
                decimal UnitCostRDfinal = 0.0000M;



                string Mysql = "SELECT TRANS_NO, TRANS_TYPE, ACC_TYPE, ENTRY_DT, TRANS_DT,"
                                 + " TRANS_UNITS, TRANS_AMT AS BIG,"
                                 + " (TRANS_UNITS * TRANS_PR) AS SMALL, CUR_UNIT_HLDG, CUM_VALUE_HLDG "
                                 + " FROM UTS.HOLDER_LEDGER "
                                 + " WHERE "
                                 + " HOLDER_NO='" + Holder_No + "' AND "
                                 + " FUND_ID='" + Fund_ID + "' AND "
                                 + " trans_type IN ('SA','RD','DD','BI','UC', 'TR') "
                                 + " AND  TRANS_DT <= to_date('" + End_date + "', 'yyyy/mm/dd') "
                                 + " ORDER BY TRANS_DT ASC,SORT_SEQ ASC, TRANS_TYPE DESC ";

                using (OracleConnection mysql_conn = new OracleConnection(Sqlconnstring))
                {
                    mysql_conn.Open();



                    using (OracleCommand comm = new OracleCommand(Mysql, mysql_conn))
                    {
                        int Count = 0; string[] numb;
                        using (OracleDataReader rdr1 = comm.ExecuteReader())
                        {

                            var dataTable = new System.Data.DataTable();
                            dataTable.Load(rdr1);

                            numb = new string[dataTable.Rows.Count];

                            foreach (System.Data.DataRow row in dataTable.Rows)
                            {
                                numb[Count++] = row[0].ToString();
                            }




                            //while (rdr1.Read())
                            //{
                            //    numb[Count++] = rdr1[0].ToString();

                            //}
                            Array.Resize(ref numb, Count);
                            //}

                            // using (OracleDataReader rdr = comm.ExecuteReader())
                            //   {


                            Count = 0;
                            string Trans_String;


                            // while (rdr.Read())
                            //{

                            foreach (System.Data.DataRow row in dataTable.Rows)
                            {
                                try
                                {

                                    Trans_String = row[0].ToString();
                                    bool TTT = false;


                                    // check reversal
                                    for (int i = Count; i < numb.Length; i++)
                                    {
                                        if (Trans_String + "X" == numb[i].ToString())
                                        {
                                            // rdr.NextResult();
                                            TTT = true;
                                            break;

                                        }

                                        char Check_reversed = Trans_String[Trans_String.Length - 1];

                                        if (Check_reversed == 'X')
                                        {
                                            TTT = true;

                                            Unit_Holding_value = Convert.ToDecimal(row[8].ToString());
                                            Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);

                                            UnitCostRD = Convert.ToDecimal(row[5].ToString());
                                            UnitCostRD = decimal.Round(UnitCostRD, 4);

                                            break;
                                        }

                                    }

                                    Count++;

                                    if (TTT == false)
                                    {


                                        Trans_Type = row[1].ToString().Trim();

                                        if (Trans_Type == "SA")
                                        {
                                            string Trans_no = row[0].ToString();
                                            char Check_reversed = Trans_no[Trans_no.Length - 1];

                                            bool bolIsIU = false;

                                            if (Convert.ToDecimal(row[6].ToString()) < 1000)
                                            {
                                                bolIsIU = true;
                                            }

                                            if (Check_reversed == 'X')
                                            {
                                                Trans_no = Trans_no.TrimEnd(Trans_no[Trans_no.Length - 1]);
                                                if (bolIsIU == false)
                                                {
                                                    Auctual_transfer_value -= Convert.ToDecimal(row[6].ToString());
                                                    Auctual_transfer_value = decimal.Round(Auctual_transfer_value, 2);
                                                }


                                                Book_value -= Convert.ToDecimal(row[7].ToString());
                                                Book_value = decimal.Round(Book_value, 2);

                                            }
                                            else
                                            {

                                                if (bolIsIU == false)
                                                {
                                                    Auctual_transfer_value += Convert.ToDecimal(row[6].ToString());
                                                    Auctual_transfer_value = decimal.Round(Auctual_transfer_value, 2);
                                                }
                                                Book_value += Convert.ToDecimal(row[7].ToString());
                                                // rounding
                                                Book_value = decimal.Round(Book_value, 2);


                                            }

                                        }
                                        if ((Trans_Type == "DD") || (Trans_Type == "BI") || (Trans_Type == "SA") || (Trans_Type == "UC"))
                                        {
                                            Unit_Holding_value = Convert.ToDecimal(row[8].ToString());
                                            Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);

                                            if ((Trans_Type == "DD") || (Trans_Type == "BI"))

                                            {
                                                Book_value += Convert.ToDecimal(row[6].ToString());
                                                // rounding
                                                Book_value = decimal.Round(Book_value, 2);

                                            }



                                        }


                                        if (Trans_Type == "RD" || Trans_Type == "TR")
                                        {


                                            // ASSAR - GORDON
                                            if ((Trans_Type == "TR") && (Unit_Holding_value > 0))
                                            {
                                                //DO NOTHING
                                                if (Holder_No == "90012")
                                                {
                                                    Holder_No = "90012";
                                                }


                                                Book_value += Convert.ToDecimal(row[7].ToString());
                                                // rounding
                                                Book_value = decimal.Round(Book_value, 2);



                                            }

                                            else //EXCLUDE tr + , COVER ONLY RD AND tr -
                                            {

                                                string Trans_noRD = row[0].ToString();
                                                char Check_reversedRD = Trans_noRD[Trans_noRD.Length - 1];

                                                // Total_Redumption_cost_forRD = Convert.ToDecimal(row[5].ToString());
                                                // Total_Redumption_cost_forRD = decimal.Round(Total_Redumption_cost, 4);

                                                //if (!(Trans_Type == "TR"))
                                                //{
                                                Total_Redumption_cost = Convert.ToDecimal(row[8].ToString());
                                                Total_Redumption_cost = decimal.Round(Total_Redumption_cost, 4);

                                                UnitCostRD = Convert.ToDecimal(row[5].ToString());
                                                UnitCostRD = decimal.Round(UnitCostRD, 4);
                                                // }


                                                UnitCostRDfinal = (Book_value / Convert.ToDecimal(Unit_Holding_value)  //  Check RDCost                                                                       * Total_Redumption_cost);
                                                                    * UnitCostRD);
                                                UnitCostRDfinal = decimal.Round(UnitCostRDfinal, 2);

                                                if (Convert.ToDecimal(Total_Redumption_cost) > 0)
                                                {
                                                    if (!(Check_reversedRD == 'X'))                             // Check reversal
                                                    {


                                                        Auctual_transfer_value = (Auctual_transfer_value / Convert.ToDecimal(Unit_Holding_value)
                                                                                   * Total_Redumption_cost);
                                                        Book_value = (Book_value / Convert.ToDecimal(Unit_Holding_value)
                                                                                  * Total_Redumption_cost);



                                                        Book_value = decimal.Round(Book_value, 2);
                                                        Auctual_transfer_value = decimal.Round(Auctual_transfer_value, 2);


                                                        // MessageBox.Show(Holder_No + "//" + Fund_ID + "//" + Auctual_transfer_value + "//" + Book_value + "//" + UnitCostRDfinal);
                                                    }

                                                }
                                                else
                                                {
                                                    Auctual_transfer_value = 0.0000M;
                                                    Book_value = 0.0000M;
                                                    //UnitCostRDfinal = 0.0000M;
                                                    Unit_Holding_value = 0.0000M;
                                                }
                                            }


                                            // ASSAR - GORDON



                                        }





                                    }

                                }
                                catch (Exception ex)
                                {
                                    Log.Write(ex.ToString() + "---Get Redemption Update failed--- NAV", End_date);
                                }




                            }
                            Actual_transfer_BookValue_update(Holder_No, Fund_ID, Auctual_transfer_value, Book_value, UnitCostRDfinal, IsInsert, Unit_Holding_value.ToString());
                            // MessageBox.Show(Holder_No + "//" + Fund_ID + "//" + Auctual_transfer_value + "//" + Book_value + "//" + UnitCostRDfinal);
                            //rdr.Close();
                            mysql_conn.Close();


                        }
                    }
                }
            }



            catch (Exception ex)
            {
                Log.Write(ex.ToString() + "---Get Final Redemption Update failed--- NAV", End_date);
            }
        }

        protected void Actual_transfer_BookValue_update(string HolderNo, string IPD_fund_Code, decimal Actual_Transfer, decimal Book_value, decimal Redumption_cost, bool IsInsert = false, string Units = "0.00")
        {


            string Query = "";
            //string ConnectionString = GetOracleConnection();
            string Sqlconnstring = GetSqlConnection();
            try
            {
                //    using (OracleConnection conn = new OracleConnection(ConnectionString))
                //  {

                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();

                    if (IsInsert)
                    {

                        string EPF_IPD_Code = ConfigurationManager.AppSettings["IPD_CODE"];
                        string Member_EPF_No = "";

                        // string Units = "0.00";

                        string Market_Value = "0.00";


                        string OracleSql = " SELECT A1.HOLDER_NO,  A1.ID_NO AS NRIC,  A1.ID_NO_2 AS EPF_NO, "
                                                         + " to_date(A1.BIRTH_DT),   A1.SEX,    A1.HOLDER_CLS,   A1.HOLDER_STATUS FROM  "
                                                         + " UTS.HOLDER_REG A1 WHERE A1.HOLDER_NO='" + HolderNo + "'";
                 
                        string ConnectionString = GetOracleConnection();


                        using (OracleConnection conn = new OracleConnection(ConnectionString))
                        {
                            conn.Open();

                            using (OracleCommand comm = new OracleCommand(OracleSql, conn))
                            {
                                using (OracleDataReader rdr = comm.ExecuteReader())
                                {
                                    while (rdr.Read())
                                    {
                                        string Passport_No = rdr[1].ToString();
                                        string EPF_No = rdr[2].ToString();
                                        Member_EPF_No = EPF_No;
                                        string NIC = "";
                                        // string NIC_Date = "";
                                        DateTime dt = Convert.ToDateTime(rdr[3].ToString());
                                        string BirthDate = dt.ToString("yyyy-MM-dd");
                                        string Gender = rdr[4].ToString();

                                        DateTime now = DateTime.Now;
                                        var startDate = new DateTime(now.Year, now.Month, 1);
                                        var endDate = startDate.AddMonths(1).AddDays(-1);
                                        string EndofMonth = endDate.ToString("yyyy-MM-dd");
                                        // Effective

                                        DateTime dt1 = Convert.ToDateTime(rdr[3].ToString());
                                        string Release_Date = dt1.ToString("yyyy-MM-dd");
                                        string year = DateTime.Now.Year.ToString();
                                        Release_Date = Release_Date.Remove(0, 4).Insert(0, year);



                                        //Query =
                                        //  "Insert into utmc_member_information(Code,Passport_No,EPF_No,NIC,Birthdate,Gender, Effective_Date_Of_Report,report_date) "
                                        // + " values('" + EPF_IPD_Code + "', '" + Passport_No + "','" + EPF_No + "','" + NIC + "','" + BirthDate + "','" + Gender + "','" + End_date + "','" + End_date + "');";

                                    }
                                }
                            }
                        }

                        Query += "\n" + "Insert into utmc_member_investment(Member_EPF_No,EPF_IPD_Code,IPD_Fund_Code,IPD_Member_Acc_No,Actual_Transferred_From_EPF_RM,Units,Book_Value,Market_Value,Effective_Date,Report_Date) "
                     + " values('" + Member_EPF_No + "','" + EPF_IPD_Code + "','" + IPD_fund_Code + "', '" + HolderNo + "','" + Actual_Transfer + "','" + Units + "','" + Book_value + "','" + Market_Value + "','" + End_date + "','" + End_date + "')";


                    }

                    else
                    {
                        Query = "UPDATE utmc_member_investment SET Actual_Transferred_From_EPF_RM='" + Actual_Transfer + "', Book_Value='" + Book_value + "', Redemption_Cost='" + Redumption_cost + "' where IPD_Member_Acc_No='" + HolderNo + "' AND IPD_Fund_Code= '" + IPD_fund_Code + "' AND Report_Date='" + End_date + "' ";


                    }

                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                    {



                        mysql_comm.ExecuteNonQuery();

                        //System.Threading.Thread.Sleep(20);
                        Log.Write(Query, End_date);
                    }
                    mysql_conn.Close();
                }
                //  }
            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString() + "/n" + Query, End_date);
            }

        }

        //db checking
        private void button1_Click_1(object sender, EventArgs e)
        {
            string ConnectionString = GetOracleConnection();
            string mysqlConnection = GetSqlConnection();
            MessageBox.Show(ConnectionString);
            try
            {
                OracleConnection connection;
                connection = new OracleConnection(ConnectionString);
                MessageBox.Show("Wait for Open Connection");
                connection.Open();
                MessageBox.Show("Connection Oracle Ready...");
                connection.Close();
                MySqlConnection con;
                con = new MySqlConnection(mysqlConnection);
                con.Open();
                MessageBox.Show("Connection Mysql Ready...");
                con.Close();
            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString(), End_date);
            }

        }

        private void btn_Daily_NavPrice_Click(object sender, EventArgs e)
        {
            Inser_Fund_Nav();
        }

        private void btn_fund_distribution_Click(object sender, EventArgs e)
        {
            Insert_Corporate_Actions();
        }

        private void btn_Fund_Information_Click(object sender, EventArgs e)
        {
            Insert_Fund_Info();
             Insert_FundInfo_Details();
            

        }

        protected string GenerateTrailerFile(string Filename)
        {
            string value = "";
            using (var md5 = new MD5CryptoServiceProvider())
            {
                var buffer = md5.ComputeHash(File.ReadAllBytes(Filename));
                var sb = new StringBuilder();
                for (int i = 0; i < buffer.Length; i++)
                {
                    sb.Append(buffer[i].ToString("x2"));
                }
                 value = sb.ToString();
            }
            return value;
        }

        // EXPORT
        private void btnGenerateTxtFile_Click(object sender, EventArgs e)
        {
           

            if ((Start_date != "") && (End_date != ""))
            {



                if (checkBox4.Checked)
                {


                    Log.Write("Generating Histrocial Report", End_date);
                    GenerateHistrocial_Report();
                    Log.Write("Generated Histrocial Report", End_date);
                    MessageBox.Show("Completed");
                }
                else if(checkBox3.Checked)
                {
                    Log.Write("Generating Histrocial Report", End_date);
                    GenerateHistrocial_Report();
                    Log.Write("Generated Histrocial Report", End_date);
                    MessageBox.Show("Completed");
                }
                else
                {

                    Process_UTMC_Purge();
                    button4_Click(sender, e);         // report 15              
                    btn_fund_distribution_Click(sender, e);
                    btn_Daily_NavPrice_Click(sender, e);
                    Insert_FundDetails_emis();
                    Insert_Holder_Account_Details();

                    //hardcode
                    DB.SQL_ExcuteQuery(" update utmc_member_investment set Actual_Transferred_From_EPF_RM=0 where Member_EPF_No = '71178181'");

                    string FundsInfoheader = @" SELECT 'EPF_IPD_CODE',
                        'EPF_FUND_CODE',
                        'IPD_FUND_CODE',
                        'FUND_NAME',
                        'SHARIAH_COMPLIANT',
                        'EPF_FUND_STATUS',
                        'INVESTMENT_OBJECTIVE_EN',
                        'INVESTMENT_OBJECTIVE_BM',
                        'LAUNCH_DATE',
                        'LAUNCH_PRICE',
                        'ANNUAL_MANAGEMENT_FEE_EN',
                        'ANNUAL_MANAGEMENT_FEE_BM',
                        'FUND_SIZE',
                        'FUND_SIZE_DATE',
                        'FUND_SIZE_CURRENCY',
                        'FUND_CURRENCY_CODE',
                        'TOTAL_UNITS_IN_CIRCULATION',
                        'TOTAL_UNITS_IN_CIRCULATION_DATE',
                        'FUND_TYPE',
                        'MIN_INITIAL_INVEST',
                        'MIN_SUBSEQUENT_INVEST',
                        'MIN_REDEMPTION',
                        'MIN_REDEMPTION_TYPE',
                        'MIN_HOLDING',
                        'MIN_HOLDING_TYPE',
                        'MIN_SWITCHING',
                        'MIN_SWITCHING_TYPE',
                        'SWITCHING_COST_EN',
                        'SWITCHING_COST_BM',
                        'REDEMPTION_COST_EN',
                        'REDEMPTION_COST_BM',
                        'ANNUAL_EXPENSE_RATIO',
                        'TRUSTEE_FEE_EN',
                        'TRUSTEE_FEE_BM',
                        'DISTRIBUTION_POLICY_EN',
                        'DISTRIBUTION_POLICY_BM',
                        'PRICING_BASIS',
                        'ANNUAL_EXPENSE_RATIO_DATE' UNION ALL ";


                    string FundsInfo = FundsInfoheader + "SELECT epf_ipd_code,epf_fund_code,ipd_fund_code,fund_name,shariah_compliant, "
                                        + " epf_fund_status,investment_objective_en,REPLACE(investment_objective_bm,'-',''),DATE_FORMAT(launch_date, '%Y-%m-%d'),launch_price,annual_management_fee_en,annual_management_fee_bm,fund_size,"
                                        + " fund_size_date,fund_size_currency,fund_currency_code,total_units_in_circulation,total_units_in_circulation_date, "
                                        + " fund_type, min_initial_invest, min_subsequent_invest, min_redemption, min_redemption_type, min_holding, min_holding_type, "
                                        + " min_switch, min_switch_type,switching_cost_en, REPLACE(switching_cost_bm,'-','') ,Redemption_cost_en,REPLACE(Redemption_cost_bm,'-','') , annual_expense_ratio, trsutee_fee_en,REPLACE(trustee_fee_bm,'-','')  , "
                                        + " distribution_policy_en, distribution_policy_bm,pricing_basis, DATE_FORMAT(Annual_expense_ratio_date, '%Y-%m-%d') from funds_info";

                    Write_Export_Data(FundsInfo, "FundsInfo", "");


                    string HeaderFundsDailyNAVPrice = @"SELECT 'EPF_IPD_CODE','EPF_FUND_CODE','NAV_DATE','NAV_PER_UNIT' UNION ALL ";

                    string FundsDailyNAVPrice = HeaderFundsDailyNAVPrice + "select A.EPF_IPD_Code,B.Fund_Code,A.Daily_NAV_Date,A.Daily_Unit_Price from utmc_daily_nav_fund A "
                                                 + " left join utmc_fund_information B on A.IPD_Fund_Code = B.IPD_Fund_Code where A.Daily_NAV_Date >= '" + Start_date + "' "
                                                  + " and A.Daily_NAV_Date <= '" + End_date + "'";

                    Write_Export_Data(FundsDailyNAVPrice, "FundsDailyNAVPrice", "");


                    string HeaderFundIncomeDistribution = @"select 'EPF_IPD_CODE','EPF_FUND_CODE','EX_DATE','PAY_DATE','NET_RATE','GROSS_RATE','TYPE','UNIT_SPLIT_RATIO' union all ";

                    string FundIncomeDistribution = HeaderFundIncomeDistribution + "select A.EPF_IPD_Code,B.Fund_Code,A.Corporate_Action_Date,A.Report_Date, CASE WHEN A.Net_Distribution IS NULL THEN '' ELSE A.Net_Distribution END AS ResultNet_Distribution, "
                                                        + " CASE WHEN A.Distributions IS NULL THEN ''  ELSE A.Distributions  END AS ResultDistributions, A.TYPE,A.Unit_Splits from utmc_fund_corporate_actions A "
                                                         + " left  join utmc_fund_information B on A.IPD_Fund_Code = B.IPD_Fund_Code where A.Corporate_Action_Date >= '" + Start_date + "' "
                                                         + " and A.Corporate_Action_Date <= '" + End_date + "' ";
                    Write_Export_Data(FundIncomeDistribution, "FundsIncomeDistribution", "");


                    string HeaderFundsSize = @"select 'EPF_IPD_CODE','EPF_FUND_CODE','FUND_SIZE','FUND_SIZE_DATE','FUND_SIZE_CURRENCY','TOTAL_UNITS_IN_CIRCULATION','TOTAL_UNITS_IN_CIRCULATION_DATE' union all ";

                    string FundsSize = HeaderFundsSize + " SELECT A.EPF_IPD_Code,B.Fund_Code,A.Daily_NAV,A.Daily_NAV_Date,'MYR',ROUND(A.Daily_Unit_Created,2),A.Daily_NAV_Date "
                                        + " FROM utmc_daily_nav_fund A "
                                        + " left join utmc_fund_information B on A.IPD_Fund_Code = B.IPD_Fund_Code "
                                        + " where A.Daily_NAV_Date = '" + End_date + "'";



                    Write_Export_Data(FundsSize, "FundsSize", "");


                    string HeaderMemberUTMCDeltaHoldings = @"select 'EPF_IPD_CODE','EPF_NO','NEW_NRIC','OLD_NRIC','OTHER_ID','OTHER_ID_TYPE','EPF_FUND_CODE','CURRENT_HOLDINGS','COST_OF_INVESTMENT','DISTRIBUTION_CHANNEL' union all ";


                    string IncativeClients = @"('121437','119965','117981','116743','116458','114466','110353',
  '109014','109006','108864','107995','107971','107782','107375')";
                    string MemberUTMCDeltaHoldings = HeaderMemberUTMCDeltaHoldings + "select A.EPF_IPD_Code,A.Member_EPF_No,REPLACE(B.ID_NO, '-', ''),B.ID_NO_OLD, CASE WHEN B.ID_NO IS NULL THEN ID_NO_2 ELSE '' END AS OTHERID,"
                                                        + "  CASE WHEN ID_NO IS NULL THEN IDENTITY_IND ELSE '' END AS OTHERIDTYPE, "
                                                        + " C.Fund_Code, "
                                                        + " ROUND(A.Current_Unit_Holding_Value,2),A.Actual_Transferred_From_EPF_RM,(SELECT distribution_channnel FROM di_otp.member_utmc_holdings WHERE status='1') as channel "
                                                        + " from utmc_member_investment A "
                                                        + " left join ma_holder_reg_emis B on A.IPD_Member_Acc_No = B.HOLDER_NO "
                                                        + " left join utmc_fund_information C on A.IPD_Fund_Code = C.IPD_Fund_Code "
                                                      + "where  A.report_date = '"+ End_date + "' and "
                                                   + " not Member_EPF_No in (select epf_no from eppa_wbr_new.utmc_member_information "
                                                   + "  where  Adjustment = 'S' "
                                                   + " and Report_Date<= '" + End_date + "' group by epf_no) "
                                                   + "  and IPD_Member_Acc_No not in " + IncativeClients + " "
                                                   + " and A.Member_EPF_No != '07112195' ";





                    Write_Export_Data(MemberUTMCDeltaHoldings, "MemberUTMCDeltaHoldings", "");

                    btn_import_Click_fn();

                    MessageBox.Show("Completed");
                }
            }
            else
            {
                MessageBox.Show("must select date!!");
            }

        }

        public void ExportAll()
        {
            button4.PerformClick();
            button2.PerformClick();
            button5.PerformClick();
            btn_fund_distribution.PerformClick();
            btn_Daily_NavPrice.PerformClick();
            btn_Fund_Information.PerformClick();
        }


        protected void Write_Export_Data(string strScript, string ReportName,string ReportType)
        {

            string ExportGRtextFile = "";

            string path = ConfigurationManager.AppSettings["Export_file_path"];
            string strIPD_Code = ConfigurationManager.AppSettings["IPD_CODE"];

            string EndDateforPMS = EndDate.Text;
            EndDateforPMS = EndDateforPMS.Replace("/", "-");

            DateTime dateend = Convert.ToDateTime(EndDateforPMS);
            EndDateforPMS = dateend.ToString(("yyyy-MM-dd"));


            ExportGRtextFile = path + ReportName + "_"+strIPD_Code+"_" + EndDateforPMS + ".txt";

            if(ReportType=="Trailer")
            {
                ExportGRtextFile = path + ReportName +"_Trailer"+ "_" + strIPD_Code + "_" + EndDateforPMS + ".txt";
            }

            ExportGRtextFile = ExportGRtextFile.Replace("\\", "/");

            if (File.Exists(ExportGRtextFile))
            {
                File.Delete(ExportGRtextFile);
            }


            string Sqlconnstring = GetSqlConnection();

            strScript = strScript +
                            " INTO OUTFILE '" + ExportGRtextFile + "'  " +
                            " FIELDS TERMINATED BY '|'  " +
                            " ENCLOSED BY '\'  " +
                            " LINES TERMINATED BY '\r\n'  ";

            try
            {

                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();

                    using (MySqlCommand comm = new MySqlCommand(strScript, mysql_conn))
                    {
                        comm.ExecuteReader();
                    }

                    mysql_conn.Close();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void btn_import_Click_fn()
        {

            string EndDateforPMS = EndDate.Text; ;
            EndDateforPMS = EndDateforPMS.Replace("/", "");
            EndDateforPMS = EndDateforPMS.Replace("-", "");



            string ImportFilePath = ConfigurationManager.AppSettings["Import_file_path"];

            string ExportFilePath = ConfigurationManager.AppSettings["Export_file_path"];


            string str = "";
            string ReadLine = "";
            string filePath = @"" + ExportFilePath + "";
            int ColumnValue = 0;
            //string Plan_type = "";

            System.IO.DirectoryInfo dr = new System.IO.DirectoryInfo(filePath);
            System.IO.FileInfo[] files = dr.GetFiles("*.txt");

            string destFolder = ExportFilePath + "\\history_emis\\" + EndDateforPMS + "\\";
            string destFolderFile = "";


            string Query = "UPDATE trailer_file SET number_of_row = 0";

            UTMC02_Update_for_Row_and_ColumnValue(Query);


            foreach (System.IO.FileInfo fie in files)
            {
                str = fie.Name;


                Log.Write(DateTime.Now.ToString() + " - start export file: " + str, End_date);

                destFolderFile = destFolder + str;

                string s = str;
                string strRptCode = s.Split('_')[0].ToString();

                System.IO.StreamReader sr = new System.IO.StreamReader(filePath + "\\" + str);

                var line = sr.ReadToEnd().Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                int Rownumber = line.Length;


                #region  update_column_Count

                for (int j = 0; j < line.Length;)
                {
                    ReadLine = line[j];
                    string[] columm1 = ReadLine.Split('|');
                    ColumnValue = columm1.Length;
                    break;
                }
                #endregion

                # region UpdateRow and Column

                string Checksum = GenerateTrailerFile(fie.FullName);

                if (strRptCode == "FundsInfo")
                    UTMC02_Update("FundsInfo", Rownumber, ColumnValue, Checksum);

                if (strRptCode == "FundsDailyNAVPrice")
                    UTMC02_Update("FundsDailyNAVPrice", Rownumber, ColumnValue, Checksum);

                if (strRptCode == "FundsIncomeDistribution")
                    UTMC02_Update("FundsIncomeDistribution", Rownumber, ColumnValue, Checksum);

                if (strRptCode == "FundsSize")
                    UTMC02_Update("FundsSize", Rownumber, ColumnValue, Checksum);

                if (strRptCode == "MemberUTMCDeltaHoldings")
                    UTMC02_Update("MemberUTMCDeltaHoldings", Rownumber, ColumnValue, Checksum);

                #endregion

                sr.Close();
                sr = null;
                string HeaderFundsDailyNAVPrice = @"SELECT 'EPF_IPD_CODE','NUMBER_OF_ROW','NUMBER_OF_COLUMN','CHECKSUM' UNION ALL ";


                string UTMC02_query = HeaderFundsDailyNAVPrice+ " SELECT epf_ipd_code,number_of_row,number_of_column,checksum FROM trailer_file where  report_name='" + strRptCode + "'";
                Write_Export_Data(UTMC02_query, strRptCode, "Trailer");


            }
           

            bool exists = System.IO.Directory.Exists(destFolder);

            if (!exists)
                System.IO.Directory.CreateDirectory(destFolder);


            System.IO.DirectoryInfo dr1 = new System.IO.DirectoryInfo(filePath);
            System.IO.FileInfo[] files1 = dr.GetFiles("*.txt");


            foreach (System.IO.FileInfo fie in files1)
            {
                destFolderFile = destFolder + fie;

                
                fie.CopyTo(destFolderFile, true);

                Log.Write(DateTime.Now.ToString() + " - copy export file to history folder: " + destFolderFile, End_date);
                fie.Delete();

            }
        }

        protected void ReadReportMeta_data(string Query, string ReportName, int Number_of_Columns)
        {
            string Checksum = "";
            string Sqlconnstring = GetSqlConnection();
            try
            {
                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();

                    using (var cmd = new MySqlCommand(Query, mysql_conn))
                    {
                        int count = Convert.ToInt32(cmd.ExecuteScalar());
                        UTMC02_Update(ReportName, count + 1, Number_of_Columns, Checksum);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

        }

        protected void UTMC02_Update(string Report_Number, int No_of_Rows, int No_of_Columns, string checksum)
        {
            string Sqlconnstring = GetSqlConnection();
            try
            {
                string End_date_format = End_date;
                End_date_format = End_date_format.Replace("/", "-");
                No_of_Rows = No_of_Rows - 1;

                string Query = "";
                if (checkBox4.Checked)
                {
                     Query = "update trailer_file_h set number_of_row='" + No_of_Rows + "',number_of_column='" + No_of_Columns + "',checksum='" + checksum + "' where report_name='" + Report_Number + "'";
                }
                else
                {
                     Query = "update trailer_file set number_of_row='" + No_of_Rows + "',number_of_column='" + No_of_Columns + "',checksum='" + checksum + "' where report_name='" + Report_Number + "'";

                }

                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();
                    using (MySqlCommand comm = new MySqlCommand(Query, mysql_conn))
                    {

                        comm.ExecuteNonQuery();
                    }

                    mysql_conn.Close();
                }

            }

            catch (Exception ex)
            {
                Log.Write(ex.ToString(), End_date);
            }
        }

        protected void UTMC02_Update_for_Row_and_ColumnValue(string Mysql)
        {
            string Sqlconnstring = GetSqlConnection();
            try
            {
                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();
                    using (MySqlCommand comm = new MySqlCommand(Mysql, mysql_conn))
                    {

                        comm.ExecuteNonQuery();
                    }

                    mysql_conn.Close();
                }
            }

            catch (Exception ex)
            {
                Log.Write(ex.ToString(), End_date);
            }
        }

        //   Holder Registration Table
        private void button5_Click_1(object sender, EventArgs e)
        {
            Insert_Holder_Account_Details();
        }

        private void Insert_Holder_Account_Details()
        {
            
            try
            {
                DS = CallService.Get_Holder_Acc_Details(End_date);
                string Sqlconnstring = DB.GetSqlConnection();

                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();
                    foreach (DataRow rdr in DS.Rows)
                    {
                        string EPF_IPD_Code = ConfigurationManager.AppSettings["IPD_CODE"];

                        string ID_NO = rdr[0].ToString();
                         string ID_NO_OLD = rdr[1].ToString();
                        string ID_NO_2 = rdr[2].ToString();
                        string ID_NO_OLD_2 = rdr[3].ToString();
                        string ID_NO_3 = rdr[4].ToString();
                        string ID_NO_OLD_3 = rdr[5].ToString();
                        string ID_NO_4 = rdr[6].ToString();
                        string ID_NO_OLD_4 = rdr[7].ToString();
                        string ID_NO_5 = rdr[8].ToString();
                        string ID_NO_OLD_5 = rdr[9].ToString();
                        string HOLDER_NO = rdr[10].ToString();
                        string TITLE = rdr[11].ToString();
                        TITLE = TITLE.Replace("'", "");
                        string NAME_1 = rdr[12].ToString();
                         NAME_1 = NAME_1.Replace("'","");
                        string NAME_2 = rdr[13].ToString();
                        NAME_2 = NAME_2.Replace("'", "");
                        NAME_2 = NAME_2.Replace("'", "");
                        string NAME_3 = rdr[14].ToString();
                        NAME_3 = NAME_3.Replace("'", "");
                        string NAME_4 = rdr[15].ToString();
                        NAME_4 = NAME_4.Replace("'", "");
                        string NAME_5 = rdr[16].ToString();
                        NAME_5 = NAME_5.Replace("'", "");
                        string ADDR_1 = rdr[17].ToString();
                        ADDR_1 = ADDR_1.Replace("'", "");
                        string ADDR_2 = rdr[18].ToString();
                        ADDR_2 = ADDR_2.Replace("'", "");
                        string ADDR_3 = rdr[19].ToString();
                        ADDR_3 = ADDR_3.Replace("'", "");
                        string ADDR_4 = rdr[20].ToString();
                        ADDR_4 = ADDR_4.Replace("'", "");
                        string POSTCODE = rdr[21].ToString();
                        string TEL_NO = rdr[22].ToString();
                        string REGION_CODE = rdr[23].ToString();
                        string STATE_CODE = rdr[24].ToString();
                        string COUNTRY_ISSUE = rdr[25].ToString();
                        COUNTRY_ISSUE = COUNTRY_ISSUE.Replace("'", "");
                        string COUNTRY_ISSUE1 = rdr[26].ToString();
                        COUNTRY_ISSUE1 = COUNTRY_ISSUE1.Replace("'", "");
                        string COUNTRY_ISSUE2 = rdr[27].ToString();
                        COUNTRY_ISSUE2 = COUNTRY_ISSUE2.Replace("'", "");
                        string COUNTRY_ISSUE3 = rdr[28].ToString();
                        COUNTRY_ISSUE3 = COUNTRY_ISSUE3.Replace("'", "");
                        string COUNTRY_RES = rdr[29].ToString();
                        COUNTRY_RES = COUNTRY_RES.Replace("'", "");
                        string COUNTRY_INCORP = rdr[30].ToString();
                        COUNTRY_INCORP = COUNTRY_INCORP.Replace("'", "");
                        string NATIONALITY = rdr[31].ToString();
                        string RACE = rdr[32].ToString();
                        string OCC_CODE = rdr[33].ToString();
                        string CORP_STATUS = rdr[34].ToString();
                        string BUSINESS_TYPE = rdr[35].ToString();
                        string CONTACT_PERSON = rdr[36].ToString();
                        CONTACT_PERSON = CONTACT_PERSON.Replace("'", "");
                        string POSITION_HELD = rdr[37].ToString();
                        POSITION_HELD = POSITION_HELD.Replace("'", "");
                        string ACCOUNT_TYPE = rdr[38].ToString();
                        string DIV_PYMT = rdr[39].ToString();
                        string HOLDER_CLS = rdr[40].ToString();
                        string HOLDER_IND = rdr[41].ToString();
                        string HOLDER_STATUS = rdr[42].ToString();
                        string REG_DT = rdr[43].ToString();
                        string REG_BRN = rdr[44].ToString();
                        string BIRTH_DT = rdr[45].ToString();
                        string SEX = rdr[46].ToString();
                        string TEL_NO_2 = rdr[47].ToString();
                        string AGENT_CODE = rdr[48].ToString();
                        string AGENT_TYPE= rdr[49].ToString();
                        string AGENT_ID = rdr[50].ToString();
                        string INCOME_TAX_NO = rdr[51].ToString();
                        string HAND_PHONE_NO = rdr[52].ToString();
                        string OFF_PHONE_NO = rdr[53].ToString();
                        string PRMNT_ADDR_1 = rdr[54].ToString();
                        PRMNT_ADDR_1 = PRMNT_ADDR_1.Replace("'", "");
                        string PRMNT_ADDR_2 = rdr[55].ToString();
                        PRMNT_ADDR_2 = PRMNT_ADDR_2.Replace("'", "");
                        string PRMNT_ADDR_3 = rdr[56].ToString();
                        PRMNT_ADDR_3 = PRMNT_ADDR_3.Replace("'", "");
                        string PRMNT_ADDR_4 = rdr[57].ToString();
                        PRMNT_ADDR_4 = PRMNT_ADDR_4.Replace("'", "");
                        string PRMNT_POST_CODE = rdr[58].ToString();
                        string PRMNT_REGION = rdr[59].ToString();
                        PRMNT_REGION = PRMNT_REGION.Replace("'", "");
                        string PRMNT_STATE = rdr[60].ToString();
                        PRMNT_STATE = PRMNT_STATE.Replace("'", "");
                        string SALUTATION = rdr[61].ToString();
                        SALUTATION = SALUTATION.Replace("'", "");
                        string H_ACC_TYPE = rdr[62].ToString();
                        string MARITAL = rdr[63].ToString();
                        string NO_OF_DPNDNT = rdr[64].ToString();
                        if (NO_OF_DPNDNT == "") NO_OF_DPNDNT = "0";
                        string RCV_MATERIAL = rdr[65].ToString();
                        RCV_MATERIAL = RCV_MATERIAL.Replace("'", "");
                        string MATERIAL_LAN = rdr[66].ToString();
                        MATERIAL_LAN = MATERIAL_LAN.Replace("'", "");
                        string FIELD_1 = rdr[67].ToString();
                        string FIELD_2 = rdr[68].ToString();
                        //string FIELD_3 = rdr[0].ToString();
                        //string FIELD_4 = rdr[0].ToString();
                        //string FIELD_5 = rdr[0].ToString();
                        //string FIELD_6 = rdr[0].ToString();
                        //string FIELD_7 = rdr[0].ToString();
                        //string FIELD_8 = rdr[0].ToString();
                        //string FIELD_9 = rdr[0].ToString();
                        //string FIELD_10 = rdr[0].ToString();
                        //string FIELD_DESC_1 = rdr[0].ToString();
                        //string FIELD_DESC_2 = rdr[0].ToString();
                        //string FIELD_DESC_3 = rdr[0].ToString();
                        //string FIELD_DESC_4 = rdr[0].ToString();
                        //string FIELD_DESC_5 = rdr[0].ToString();
                        //string FIELD_DESC_6 = rdr[0].ToString();
                        //string FIELD_DESC_7 = rdr[0].ToString();
                        //string FIELD_DESC_8 = rdr[0].ToString();
                        //string FIELD_DESC_9 = rdr[0].ToString();
                        //string FIELD_DESC_10 = rdr[0].ToString();
                        //string LAST_DISB_DT = rdr[0].ToString();
                        //string NEXT_DISB_DT = rdr[0].ToString();
                        //string PEND_DISB_DT = rdr[0].ToString();
                        string DOWNLOAD_IND = rdr[90].ToString();
                        string IDENTITY_IND = rdr[91].ToString();
                        // string EPF_I_STATUS = rdr[91].ToString();
                        //string EPF_I_EFF_DT = rdr[0].ToString();
                        //string EPF_I_LST_UPD_DT = rdr[0].ToString();
                        //string ENT_DT = rdr[0].ToString();
                        //string ENT_TIME = rdr[0].ToString();
                        //string ENT_TERM = rdr[0].ToString();
                        //string ENT_BY = rdr[0].ToString();
                        //string CHG_DT = rdr[0].ToString();
                        //string CHG_TIME = rdr[0].ToString();
                        //string CHG_TERM = rdr[0].ToString();
                        //string CHG_BY = rdr[0].ToString();


                        string Query = "INSERT INTO ma_holder_reg_emis ( ID_NO, "
                                     + " ID_NO_OLD,"
                                     + " ID_NO_2,ID_NO_OLD_2,ID_NO_3,ID_NO_OLD_3,ID_NO_4,ID_NO_OLD_4,ID_NO_5,ID_NO_OLD_5,HOLDER_NO,TITLE,NAME_1,NAME_2,NAME_3,NAME_4,NAME_5,ADDR_1,ADDR_2, "
                                    + " ADDR_3,ADDR_4,POSTCODE,TEL_NO,REGION_CODE,STATE_CODE,COUNTRY_ISSUE,COUNTRY_ISSUE1,COUNTRY_ISSUE2,COUNTRY_ISSUE3,COUNTRY_RES,COUNTRY_INCORP,NATIONALITY, "
                                    + " RACE,OCC_CODE,CORP_STATUS,BUSINESS_TYPE,CONTACT_PERSON,POSITION_HELD,ACCOUNT_TYPE,DIV_PYMT,HOLDER_CLS,HOLDER_IND,HOLDER_STATUS,REG_DT,REG_BRN,BIRTH_DT, "
                                    + " SEX,TEL_NO_2,AGENT_CODE,AGENT_TYPE,AGENT_ID,INCOME_TAX_NO,HAND_PHONE_NO,OFF_PHONE_NO,PRMNT_ADDR_1,PRMNT_ADDR_2,PRMNT_ADDR_3,PRMNT_ADDR_4,PRMNT_POST_CODE,"
                                    + " PRMNT_REGION,PRMNT_STATE,SALUTATION,H_ACC_TYPE,MARITAL,NO_OF_DPNDNT,RCV_MATERIAL,MATERIAL_LAN,FIELD_1,FIELD_2,DOWNLOAD_IND,IDENTITY_IND) "
                                    + " VALUES ('" + ID_NO + "','" + ID_NO_OLD + "','" + ID_NO_2 + "','" + ID_NO_OLD_2 + "','" + ID_NO_3 + "','" + ID_NO_OLD_3 + "','" + ID_NO_4 + "','" + ID_NO_OLD_4 + "',"
                                    + " '" + ID_NO_5 + "','" + ID_NO_OLD_5 + "','" + HOLDER_NO + "','" + TITLE + "','" + NAME_1 + "','" + NAME_2 + "','" + NAME_3 + "','" + NAME_4 + "','" + NAME_5 + "',"
                                    + " '" + ADDR_1 + "','" + ADDR_2 + "','" + ADDR_3 + "','" + ADDR_4 + "','" + POSTCODE + "','" + TEL_NO + "','" + REGION_CODE + "','" + STATE_CODE + "','" + COUNTRY_ISSUE + "',"
                                    + " '" + COUNTRY_ISSUE1 + "','" + COUNTRY_ISSUE2 + "','" + COUNTRY_ISSUE3 + "','" + COUNTRY_RES + "','" + COUNTRY_INCORP + "','" + NATIONALITY + "','" + RACE + "','" + OCC_CODE + "','" + CORP_STATUS + "',"
                                    + " '" + BUSINESS_TYPE + "','" + CONTACT_PERSON + "','" + POSITION_HELD + "','" + ACCOUNT_TYPE + "','" + DIV_PYMT + "','" + HOLDER_CLS + "','" + HOLDER_IND + "','" + HOLDER_STATUS + "','" + REG_DT + "',"
                                    + " '" + REG_BRN + "','" + BIRTH_DT + "','" + SEX + "','" + TEL_NO_2 + "','" + AGENT_CODE + "','" + AGENT_TYPE + "','" + AGENT_ID + "','" + INCOME_TAX_NO + "','" + HAND_PHONE_NO + "',"
                                    + " '" + OFF_PHONE_NO + "','" + PRMNT_ADDR_1 + "','" + PRMNT_ADDR_2 + "','" + PRMNT_ADDR_3 + "','" + PRMNT_ADDR_4 + "','" + PRMNT_POST_CODE + "','" + PRMNT_REGION + "','" + PRMNT_STATE + "','" + SALUTATION + "',"
                                    + " '" + H_ACC_TYPE + "','" + MARITAL + "','" + NO_OF_DPNDNT + "','" + RCV_MATERIAL + "','" + MATERIAL_LAN + "','" + FIELD_1 + "','" + FIELD_2 + "','" + DOWNLOAD_IND + "','" + IDENTITY_IND + "')";

                        using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                        {


                            string Value = DB.Find_Value_exists("select * from ma_holder_reg_emis where ID_NO='" + ID_NO + "' and HOLDER_NO='" + HOLDER_NO + "'");
                            if (Value == "0")
                                mysql_comm.ExecuteNonQuery();
                            Log.Write("Holder Acc Details Inserted :", End_date);
                        }
                    }

                    mysql_conn.Close();
                }
            }
            catch(Exception ex)
            {
                Log.Write("Insert Holder Acc Details failed :"+ ex.ToString(), End_date);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
          //  InitTimer();
        }

        protected void ConvertZip()
        {           
            try
            {

           

                /*
                // Create an instance of the ZipForge class
                ZipForge archiver = new ZipForge();

                try
                {
                    // Set the name of the archive file we want to create
                    archiver.FileName = @"C:\test.zip";
                    // Because we create a new archive, 
                    // we set fileMode to System.IO.FileMode.Create
                    archiver.OpenArchive(System.IO.FileMode.Create);
                    // Set base (default) directory for all archive operations
                    archiver.BaseDir = @"C:\";
                    // Set encryption algorithm and password
                    archiver.EncryptionAlgorithm = EncryptionAlgorithm.PkzipClassic;
                   // archiver.Password = "my_password";
                    // Add the c:\file.txt file to the archive 
                    // by specifying its absolute path
                    archiver.AddFiles(@"C:\file.txt");
                    archiver.CloseArchive();
                }
                // Catch all exceptions of the ArchiverException type
                catch (ArchiverException ae)
                {
                    Console.WriteLine("Message: {0}\t Error code: {1}",
                                      ae.Message, ae.ErrorCode);
                    // Wait for the  key to be pressed
                    Console.ReadLine();
                }

                       */

                //string[] filename = Directory.GetFiles("E:\\EPPA\\AWS\\TOINTG\\ARCHIVE");
                //string startPath = @"c:\example\start";
                //string zipPath = @"c:\example\result.zip";


                //ZipFile.CreateFromDirectory(startPath, zipPath);


                ////Creates a new, blank zip file to work with - the file will be
                ////finalized when the using statement completes
                //using (ZipArchive newFile = ZipFile.Open(zipName, ZipArchiveMode.Create))
                //{
                //    foreach (string file in Directory.GetFiles(myPath))
                //    {
                //        newFile.CreateEntryFromFile(file, zipPath);
                //    }
                //}

                //using (ZipFile zip = new ZipFile())
                //{
                //    zip.AddFiles(filename, "file");
                //    zip.Encryption= EncryptionAlgorithm.PkzipWeak
                //    zip.Save("E:\\EPPA\\AWS\\TOINTG\\ARCHIVE\\1.zip");               
                //}


            }
            catch (Exception ex)
            {
             string Vale=ex.Message;
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            ConvertZip();
        }

        private void button4_Click_1(object sender, EventArgs e)
        {

        }

        private void btn_refersh_Click(object sender, EventArgs e)
        {
 
            //
            // Dialog box with two buttons: yes and no. [3]
            //
            DialogResult result1 = MessageBox.Show("Are you sure you want to purge all UTMC report data between " + Start_date + " and " + End_date + " ?",
"Critical Warning",
MessageBoxButtons.OKCancel,
MessageBoxIcon.Warning,
MessageBoxDefaultButton.Button2,
MessageBoxOptions.RightAlign,
true);

            if (result1 == DialogResult.OK)
            {
                Process_UTMC_Purge();
                MessageBox.Show("Successfully purge all UTMC report data between " + Start_date + " and " + End_date + ".", "Purge_UTMC", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            else
            {

            }
        }

        private void Process_UTMC_Purge()
        {
            string Mysql = "";
            string Sqlconnstring = GetSqlConnection();
            try
            {
                Mysql = "SET SQL_SAFE_UPDATES = 0; "
                            + "DELETE   from  utmc_member_investment where  Report_Date >= '" + Start_date + "'   AND Report_Date <= '" + End_date + "'; "
                            + "DELETE   from  utmc_compositional_transactions where  Report_Date >= '" + Start_date + "'   AND Report_Date <= '" + End_date + "'; "
                            + "DELETE   from  ma_holder_reg_emis; "
                            + "DELETE   from  utmc_daily_nav_fund where  Daily_NAV_Date >= '" + Start_date + "'   AND Daily_NAV_Date <= '" + End_date + "'; "
                            + " ";




                string ReportDt = End_date;
                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();


                    using (MySqlCommand mysql_comm = new MySqlCommand(Mysql, mysql_conn))
                    {
                        mysql_comm.ExecuteNonQuery();
                        Log.Write("UTMC_Purge : " + Mysql + " - SUCCESSFULLY", End_date);
                    }


                    mysql_conn.Close();
                }

            }
            catch (Exception ex)
            {
                Log.Write("UTMC_Purge : " + Mysql + " - UNSUCCESSFULLY", End_date);
                MessageBox.Show(ex.Message);

            }

        }



        protected void GenerateHistrocial_Report()
        {


            string HistricalStartDate = StartDate.Text;
            HistricalStartDate = HistricalStartDate.Replace("/", "-");
            string HistricalStartDateEndDAte = EndDate.Text;
            HistricalStartDateEndDAte = HistricalStartDateEndDAte.Replace("/", "-");

            Process_UTMC_Purge_Histrocial();




            Generate_Report15_Histrocial();
            Insert_Corporate_Actions_h();
            Inser_Fund_Nav_Histrocial();
            if(!checkBox3.Checked)
            Insert_Holder_Account_Details_Histrocial();



            HistricalStartDate = HistricalStartDate.Replace("/", "-");
            HistricalStartDateEndDAte = HistricalStartDateEndDAte.Replace("/", "-");


            DB.SQL_ExcuteQuery(" update utmc_member_investment_h set Actual_Transferred_From_EPF_RM=0 where Member_EPF_No = '71178181'");

     


            string HeaderFundsDailyNAVPrice = @"SELECT 'EPF_IPD_CODE','EPF_FUND_CODE','NAV_DATE','NAV_PER_UNIT' UNION ALL ";

            string FundsDailyNAVPrice = HeaderFundsDailyNAVPrice + "select A.EPF_IPD_Code,B.Fund_Code,A.Daily_NAV_Date,A.Daily_Unit_Price from utmc_daily_nav_fund_h A "
                                         + " left join utmc_fund_information B on A.IPD_Fund_Code = B.IPD_Fund_Code where A.Daily_NAV_Date >= '" + HistricalStartDate + "' "
                                          + " and A.Daily_NAV_Date <= '" + HistricalStartDateEndDAte + "'";

            Write_Export_Data(FundsDailyNAVPrice, "FundsDailyNAVPrice", "");


            string HeaderFundIncomeDistribution = @"select 'EPF_IPD_CODE','EPF_FUND_CODE','EX_DATE','PAY_DATE','NET_RATE','GROSS_RATE','TYPE','UNIT_SPLIT_RATIO' union all ";

            string FundIncomeDistribution = HeaderFundIncomeDistribution + "select A.EPF_IPD_Code,B.Fund_Code,A.Corporate_Action_Date,A.Report_Date,CASE WHEN A.Net_Distribution IS NULL THEN '' ELSE A.Net_Distribution END AS ResultNet_Distribution, "
                                                + " CASE WHEN A.Distributions IS NULL THEN ''  ELSE A.Distributions  END AS ResultDistributions, A.TYPE,A.Unit_Splits from utmc_fund_corporate_actions A "
                                                 + " left  join utmc_fund_information B on A.IPD_Fund_Code = B.IPD_Fund_Code where A.Corporate_Action_Date >= '" + HistricalStartDate + "' "
                                                 + " and A.Corporate_Action_Date <= '" + HistricalStartDateEndDAte + "' ";
            Write_Export_Data(FundIncomeDistribution, "FundsIncomeDistribution", "");



            string HeaderMemberUTMCDeltaHoldings = @"select 'EPF_IPD_CODE','EPF_NO','NEW_NRIC','OLD_NRIC','OTHER_ID','OTHER_ID_TYPE','EPF_FUND_CODE','CURRENT_HOLDINGS','COST_OF_INVESTMENT','DISTRIBUTION_CHANNEL' union all ";


            string IncativeClients = @"('121437','119965','117981','116743','116458','114466','110353',
  '109014','109006','108864','107995','107971','107782','107375')";
            string MemberUTMCDeltaHoldings = HeaderMemberUTMCDeltaHoldings + "select A.EPF_IPD_Code,A.Member_EPF_No,REPLACE(B.ID_NO, '-', ''),B.ID_NO_OLD, CASE WHEN B.ID_NO IS NULL THEN ID_NO_2 ELSE '' END AS OTHERID,"
                                                + "  CASE WHEN ID_NO IS NULL THEN IDENTITY_IND ELSE '' END AS OTHERIDTYPE, "
                                                + " C.Fund_Code, "
                                                + " ROUND(A.Current_Unit_Holding_Value,2),A.Actual_Transferred_From_EPF_RM,(SELECT distribution_channnel FROM di_otp.member_utmc_holdings WHERE status='1') as channel "
                                                + " from utmc_member_investment_h A "
                                                + " left join ma_holder_reg_emis_h B on A.IPD_Member_Acc_No = B.HOLDER_NO "
                                                + " left join utmc_fund_information C on A.IPD_Fund_Code = C.IPD_Fund_Code "
                                              + "where  A.report_date = '"+ HistricalStartDateEndDAte + "' and "
                                           + " not Member_EPF_No in (select epf_no from eppa_wbr_new.utmc_member_information "
                                           + "  where  Adjustment = 'S' "
                                           + " and Report_Date<= '"+ HistricalStartDateEndDAte + "' group by epf_no) "
                                           + "  and IPD_Member_Acc_No not in " + IncativeClients + " "
                                           + " and A.Member_EPF_No != '07112195' ";



            Write_Export_Data(MemberUTMCDeltaHoldings, "MemberUTMCFullHoldings", "");

            btn_import_Click_fn_Histrocial();
        }

        private void Generate_Report15_Histrocial()
        {

            string IPD_Fund_Code = "";
            string IPD_Member_Acc_No = "";

            try
            {

                string Sqlconnstring = GetSqlConnection();


                string HistricalStartDate = StartDate.Text;
                HistricalStartDate = HistricalStartDate.Replace("/", "-");
                string HistricalStartDateEndDAte = EndDate.Text;
                HistricalStartDateEndDAte = HistricalStartDateEndDAte.Replace("/", "-");
                Log.Write("UTMC_015-H :Processing ", HistricalStartDateEndDAte);


                if(!(checkBox3.Checked))
                DS = CallService.Get_Member_Invesment_Histrocial(HistricalStartDate, HistricalStartDateEndDAte, BirthDateupdate);

                Int64 value = 0;
                try
                {
                    value = DS.Rows.Count;
                }
                catch (Exception ex)
                {
                    value = -1;
                    Log.Write("UTMC_015-H :Done " + value + "---------" + ex.ToString(), HistricalStartDateEndDAte);
                }

                Log.Write("UTMC_015-H :Done ", HistricalStartDateEndDAte);

                string EPF_IPD_Code = ConfigurationManager.AppSettings["IPD_CODE"];


                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();

                    foreach (DataRow rdr in DS.Rows)
                    {
                        string Member_EPF_No = rdr[0].ToString();

                        IPD_Fund_Code = rdr[1].ToString();
                        IPD_Member_Acc_No = rdr[2].ToString();
                        string Actual_Transferred_From_EPF_RM = rdr[4].ToString();
                        string Units = rdr[6].ToString();
                        string Book_Value = rdr[4].ToString();
                        string Market_Value = rdr[11].ToString();
                        string CurrentUnitValue = rdr[6].ToString();

                        // adjustment 2016 09

                        if (HistricalStartDateEndDAte == "2016/07/31")
                        {
                            if (IPD_Fund_Code == "06")
                            {

                                decimal Market_Value_TT = 0.00M;
                                decimal Units_TT = Convert.ToDecimal(Units.ToString());
                                Market_Value_TT = (Units_TT * Convert.ToDecimal("0.2021".ToString()));
                                Market_Value_TT = decimal.Round(Market_Value_TT, 4);
                                Market_Value = Convert.ToString(Market_Value_TT.ToString());

                            }
                        }

                        // adjustment 2016 09

                        DateTime dt = Convert.ToDateTime(rdr[5].ToString());
                        string TransDate = dt.ToString("yyyy-MM-dd");

                        DateTime now = Convert.ToDateTime(rdr[5].ToString());
                        var startDate = new DateTime(now.Year, now.Month, 1);
                        var endDate = startDate.AddMonths(1).AddDays(-1);
                        string Effective_Date = HistricalStartDateEndDAte;

                        string Query = "Insert into utmc_member_investment_h(Member_EPF_No,EPF_IPD_Code,IPD_Fund_Code,IPD_Member_Acc_No,Actual_Transferred_From_EPF_RM,Units,Book_Value,Market_Value,Effective_Date,Report_Date,Current_Unit_Holding_Value) "
                       + " values('" + Member_EPF_No + "','" + EPF_IPD_Code + "','" + IPD_Fund_Code + "', '" + IPD_Member_Acc_No + "','" + Actual_Transferred_From_EPF_RM + "','" + Units + "','" + Book_Value + "','" + Market_Value + "','" + HistricalStartDateEndDAte + "','" + HistricalStartDateEndDAte + "','" + CurrentUnitValue + "')";

                        using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                        {

                            string Value = DB.Find_Value_exists("select * from utmc_member_investment_h where Effective_Date='" + HistricalStartDateEndDAte + "' and IPD_Fund_Code='" + IPD_Fund_Code + "' AND IPD_Member_Acc_No='" + IPD_Member_Acc_No + "'");
                            if (Value == "0")
                                mysql_comm.ExecuteNonQuery();
                            Log.Write("UTMC_015 : " + IPD_Fund_Code + IPD_Fund_Code + IPD_Member_Acc_No + "Inserted", End_date);

                            //System.Threading.Thread.Sleep(20);
                        }
                    }

                    mysql_conn.Close();
                }
                System.Threading.Thread.Sleep(2000);

                ReadHolderNoforutmc15_h();
            }

            catch (Exception ex)
            {
                Log.Write("UTMC_015 : " + IPD_Fund_Code + IPD_Member_Acc_No + " - Insert Failed", End_date);

                ex.ToString();
            }
        }

        protected void ReadHolderNoforutmc15_h()
        {

            string Sqlconnstring = GetSqlConnection();
            try
            {
                string Mysql = "SELECT IPD_Member_Acc_No,IPD_Fund_Code FROM utmc_member_investment_h where Report_date=' " + End_date + "'";  //
                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();




                    using (MySqlCommand comm = new MySqlCommand(Mysql, mysql_conn))
                    {
                        using (MySqlDataReader rdr = comm.ExecuteReader())
                        {



                            var dataTable = new System.Data.DataTable();
                            dataTable.Load(rdr);

                            rdr.Close();
                            mysql_conn.Close();

                            foreach (System.Data.DataRow row in dataTable.Rows)
                            {
                                string EPF_No = row[0].ToString();
                                string IPD_fund_Code = row[1].ToString();
                                Get_Redumptiondate_h(EPF_No, IPD_fund_Code);

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString() + "---Get Final Read Holder No From utmc_member_investment failed--- timeout", End_date);
            }

        }

        protected void Get_Redumptiondate_h(string Holder_No, string Fund_ID, bool IsInsert = false)
        {

            string Sqlconnstring = GetOracleConnection();
            try
            {

                decimal Auctual_transfer_value = 0.0000M;
                decimal Book_value = 0.0000M;
                decimal Unit_Holding_value = 0.0000M;
                string Trans_Type = "";
                decimal Total_Redumption_cost = 0.0000M;
                decimal UnitCostRD = 0.0000M;
                decimal UnitCostRDfinal = 0.0000M;



                string Mysql = "SELECT TRANS_NO, TRANS_TYPE, ACC_TYPE, ENTRY_DT, TRANS_DT,"
                                 + " TRANS_UNITS, TRANS_AMT AS BIG,"
                                 + " (TRANS_UNITS * TRANS_PR) AS SMALL, CUR_UNIT_HLDG, CUM_VALUE_HLDG "
                                 + " FROM UTS.HOLDER_LEDGER "
                                 + " WHERE "
                                 + " HOLDER_NO='" + Holder_No + "' AND "
                                 + " FUND_ID='" + Fund_ID + "' AND "
                                 + " trans_type IN ('SA','RD','DD','BI','UC', 'TR') "
                                 + " AND  TRANS_DT <= to_date('" + End_date + "', 'yyyy/mm/dd') "
                                 + " ORDER BY TRANS_DT ASC,SORT_SEQ ASC, TRANS_TYPE DESC ";

                using (OracleConnection mysql_conn = new OracleConnection(Sqlconnstring))
                {
                    mysql_conn.Open();



                    using (OracleCommand comm = new OracleCommand(Mysql, mysql_conn))
                    {
                        int Count = 0; string[] numb;
                        using (OracleDataReader rdr1 = comm.ExecuteReader())
                        {

                            var dataTable = new System.Data.DataTable();
                            dataTable.Load(rdr1);

                            numb = new string[dataTable.Rows.Count];

                            foreach (System.Data.DataRow row in dataTable.Rows)
                            {
                                numb[Count++] = row[0].ToString();
                            }




                            //while (rdr1.Read())
                            //{
                            //    numb[Count++] = rdr1[0].ToString();

                            //}
                            Array.Resize(ref numb, Count);
                            //}

                            // using (OracleDataReader rdr = comm.ExecuteReader())
                            //   {


                            Count = 0;
                            string Trans_String;


                            // while (rdr.Read())
                            //{

                            foreach (System.Data.DataRow row in dataTable.Rows)
                            {
                                try
                                {

                                    Trans_String = row[0].ToString();
                                    bool TTT = false;


                                    // check reversal
                                    for (int i = Count; i < numb.Length; i++)
                                    {
                                        if (Trans_String + "X" == numb[i].ToString())
                                        {
                                            // rdr.NextResult();
                                            TTT = true;
                                            break;

                                        }

                                        char Check_reversed = Trans_String[Trans_String.Length - 1];

                                        if (Check_reversed == 'X')
                                        {
                                            TTT = true;

                                            Unit_Holding_value = Convert.ToDecimal(row[8].ToString());
                                            Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);

                                            UnitCostRD = Convert.ToDecimal(row[5].ToString());
                                            UnitCostRD = decimal.Round(UnitCostRD, 4);

                                            break;
                                        }

                                    }

                                    Count++;

                                    if (TTT == false)
                                    {


                                        Trans_Type = row[1].ToString().Trim();

                                        if (Trans_Type == "SA")
                                        {
                                            string Trans_no = row[0].ToString();
                                            char Check_reversed = Trans_no[Trans_no.Length - 1];

                                            bool bolIsIU = false;

                                            if (Convert.ToDecimal(row[6].ToString()) < 1000)
                                            {
                                                bolIsIU = true;
                                            }

                                            if (Check_reversed == 'X')
                                            {
                                                Trans_no = Trans_no.TrimEnd(Trans_no[Trans_no.Length - 1]);
                                                if (bolIsIU == false)
                                                {
                                                    Auctual_transfer_value -= Convert.ToDecimal(row[6].ToString());
                                                    Auctual_transfer_value = decimal.Round(Auctual_transfer_value, 2);
                                                }


                                                Book_value -= Convert.ToDecimal(row[7].ToString());
                                                Book_value = decimal.Round(Book_value, 2);

                                            }
                                            else
                                            {

                                                if (bolIsIU == false)
                                                {
                                                    Auctual_transfer_value += Convert.ToDecimal(row[6].ToString());
                                                    Auctual_transfer_value = decimal.Round(Auctual_transfer_value, 2);
                                                }
                                                Book_value += Convert.ToDecimal(row[7].ToString());
                                                // rounding
                                                Book_value = decimal.Round(Book_value, 2);


                                            }

                                        }
                                        if ((Trans_Type == "DD") || (Trans_Type == "BI") || (Trans_Type == "SA") || (Trans_Type == "UC"))
                                        {
                                            Unit_Holding_value = Convert.ToDecimal(row[8].ToString());
                                            Unit_Holding_value = decimal.Round(Unit_Holding_value, 4);

                                            if ((Trans_Type == "DD") || (Trans_Type == "BI"))

                                            {
                                                Book_value += Convert.ToDecimal(row[6].ToString());
                                                // rounding
                                                Book_value = decimal.Round(Book_value, 2);

                                            }



                                        }


                                        if (Trans_Type == "RD" || Trans_Type == "TR")
                                        {


                                            // ASSAR - GORDON
                                            if ((Trans_Type == "TR") && (Unit_Holding_value > 0))
                                            {
                                                //DO NOTHING
                                                if (Holder_No == "90012")
                                                {
                                                    Holder_No = "90012";
                                                }


                                                Book_value += Convert.ToDecimal(row[7].ToString());
                                                // rounding
                                                Book_value = decimal.Round(Book_value, 2);



                                            }

                                            else //EXCLUDE tr + , COVER ONLY RD AND tr -
                                            {

                                                string Trans_noRD = row[0].ToString();
                                                char Check_reversedRD = Trans_noRD[Trans_noRD.Length - 1];

                                                // Total_Redumption_cost_forRD = Convert.ToDecimal(row[5].ToString());
                                                // Total_Redumption_cost_forRD = decimal.Round(Total_Redumption_cost, 4);

                                                //if (!(Trans_Type == "TR"))
                                                //{
                                                Total_Redumption_cost = Convert.ToDecimal(row[8].ToString());
                                                Total_Redumption_cost = decimal.Round(Total_Redumption_cost, 4);

                                                UnitCostRD = Convert.ToDecimal(row[5].ToString());
                                                UnitCostRD = decimal.Round(UnitCostRD, 4);
                                                // }


                                                UnitCostRDfinal = (Book_value / Convert.ToDecimal(Unit_Holding_value)  //  Check RDCost                                                                       * Total_Redumption_cost);
                                                                    * UnitCostRD);
                                                UnitCostRDfinal = decimal.Round(UnitCostRDfinal, 2);

                                                if (Convert.ToDecimal(Total_Redumption_cost) > 0)
                                                {
                                                    if (!(Check_reversedRD == 'X'))                             // Check reversal
                                                    {


                                                        Auctual_transfer_value = (Auctual_transfer_value / Convert.ToDecimal(Unit_Holding_value)
                                                                                   * Total_Redumption_cost);
                                                        Book_value = (Book_value / Convert.ToDecimal(Unit_Holding_value)
                                                                                  * Total_Redumption_cost);



                                                        Book_value = decimal.Round(Book_value, 2);
                                                        Auctual_transfer_value = decimal.Round(Auctual_transfer_value, 2);


                                                        // MessageBox.Show(Holder_No + "//" + Fund_ID + "//" + Auctual_transfer_value + "//" + Book_value + "//" + UnitCostRDfinal);
                                                    }

                                                }
                                                else
                                                {
                                                    Auctual_transfer_value = 0.0000M;
                                                    Book_value = 0.0000M;
                                                    //UnitCostRDfinal = 0.0000M;
                                                    Unit_Holding_value = 0.0000M;
                                                }
                                            }


                                            // ASSAR - GORDON



                                        }





                                    }

                                }
                                catch (Exception ex)
                                {
                                    Log.Write(ex.ToString() + "---Get Redemption Update failed--- NAV", End_date);
                                }




                            }
                            Actual_transfer_BookValue_update_h(Holder_No, Fund_ID, Auctual_transfer_value, Book_value, UnitCostRDfinal, IsInsert, Unit_Holding_value.ToString());
                            // MessageBox.Show(Holder_No + "//" + Fund_ID + "//" + Auctual_transfer_value + "//" + Book_value + "//" + UnitCostRDfinal);
                            //rdr.Close();
                            mysql_conn.Close();


                        }
                    }
                }
            }



            catch (Exception ex)
            {
                Log.Write(ex.ToString() + "---Get Final Redemption Update failed--- NAV", End_date);
            }
        }

        protected void Actual_transfer_BookValue_update_h(string HolderNo, string IPD_fund_Code, decimal Actual_Transfer, decimal Book_value, decimal Redumption_cost, bool IsInsert = false, string Units = "0.00")
        {


            string Query = "";
            //string ConnectionString = GetOracleConnection();
            string Sqlconnstring = GetSqlConnection();
            try
            {
                //    using (OracleConnection conn = new OracleConnection(ConnectionString))
                //  {

                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();

                    if (IsInsert)
                    {

                        string EPF_IPD_Code = ConfigurationManager.AppSettings["IPD_CODE"];
                        string Member_EPF_No = "";

                        // string Units = "0.00";

                        string Market_Value = "0.00";


                        string OracleSql = " SELECT A1.HOLDER_NO,  A1.ID_NO AS NRIC,  A1.ID_NO_2 AS EPF_NO, "
                                                         + " to_date(A1.BIRTH_DT),   A1.SEX,    A1.HOLDER_CLS,   A1.HOLDER_STATUS FROM  "
                                                         + " UTS.HOLDER_REG A1 WHERE A1.HOLDER_NO='" + HolderNo + "'";

                        string ConnectionString = GetOracleConnection();


                        using (OracleConnection conn = new OracleConnection(ConnectionString))
                        {
                            conn.Open();

                            using (OracleCommand comm = new OracleCommand(OracleSql, conn))
                            {
                                using (OracleDataReader rdr = comm.ExecuteReader())
                                {
                                    while (rdr.Read())
                                    {
                                        string Passport_No = rdr[1].ToString();
                                        string EPF_No = rdr[2].ToString();
                                        Member_EPF_No = EPF_No;
                                        string NIC = "";
                                        // string NIC_Date = "";
                                        DateTime dt = Convert.ToDateTime(rdr[3].ToString());
                                        string BirthDate = dt.ToString("yyyy-MM-dd");
                                        string Gender = rdr[4].ToString();

                                        DateTime now = DateTime.Now;
                                        var startDate = new DateTime(now.Year, now.Month, 1);
                                        var endDate = startDate.AddMonths(1).AddDays(-1);
                                        string EndofMonth = endDate.ToString("yyyy-MM-dd");
                                        // Effective

                                        DateTime dt1 = Convert.ToDateTime(rdr[3].ToString());
                                        string Release_Date = dt1.ToString("yyyy-MM-dd");
                                        string year = DateTime.Now.Year.ToString();
                                        Release_Date = Release_Date.Remove(0, 4).Insert(0, year);



                                        //Query =
                                        //  "Insert into utmc_member_information(Code,Passport_No,EPF_No,NIC,Birthdate,Gender, Effective_Date_Of_Report,report_date) "
                                        // + " values('" + EPF_IPD_Code + "', '" + Passport_No + "','" + EPF_No + "','" + NIC + "','" + BirthDate + "','" + Gender + "','" + End_date + "','" + End_date + "');";

                                    }
                                }
                            }
                        }

                        Query += "\n" + "Insert into utmc_member_investment_h(Member_EPF_No,EPF_IPD_Code,IPD_Fund_Code,IPD_Member_Acc_No,Actual_Transferred_From_EPF_RM,Units,Book_Value,Market_Value,Effective_Date,Report_Date) "
                     + " values('" + Member_EPF_No + "','" + EPF_IPD_Code + "','" + IPD_fund_Code + "', '" + HolderNo + "','" + Actual_Transfer + "','" + Units + "','" + Book_value + "','" + Market_Value + "','" + End_date + "','" + End_date + "')";


                    }

                    else
                    {
                        Query = "UPDATE utmc_member_investment_h SET Actual_Transferred_From_EPF_RM='" + Actual_Transfer + "', Book_Value='" + Book_value + "', Redemption_Cost='" + Redumption_cost + "' where IPD_Member_Acc_No='" + HolderNo + "' AND IPD_Fund_Code= '" + IPD_fund_Code + "' AND Report_Date='" + End_date + "' ";


                    }

                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                    {



                        mysql_comm.ExecuteNonQuery();

                        //System.Threading.Thread.Sleep(20);
                        Log.Write(Query, End_date);
                    }
                    mysql_conn.Close();
                }
                //  }
            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString() + "/n" + Query, End_date);
            }

        }

        protected void Insert_Corporate_Actions_h()
        {
            DS = CallService.Get_Corporate_Actions();
            string Sqlconnstring = DB.GetSqlConnection();
            using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
            {
                mysql_conn.Open();
                foreach (DataRow rdr in DS.Rows)
                {
                    string EPF_IPD_Code = ConfigurationManager.AppSettings["IPD_CODE"];
                    string IPD_Fund_Code = rdr[0].ToString();
                    DateTime dt = Convert.ToDateTime(rdr[2].ToString());
                    string Corporate_Action_Date = dt.ToString("yyyy-MM-dd");

                    var firstDayOfMonth = new DateTime(dt.Year, dt.Month, 1);
                    var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                    string End_date = lastDayOfMonth.ToString("yyyy-MM-dd");

                    decimal Distributions = Convert.ToDecimal(rdr[1].ToString());
                    Distributions = decimal.Round(Distributions, 4);
                    string Unit_Splits = "";

                    decimal Net_Distrubutions = Convert.ToDecimal(rdr[3].ToString());
                    Net_Distrubutions = decimal.Round(Net_Distrubutions, 4);

                    DateTime dt1 = Convert.ToDateTime(rdr[4].ToString());
                    string ReinstatementDate = dt1.ToString("yyyy-MM-dd");


                    string Query = "Insert into utmc_fund_corporate_actions(EPF_IPD_Code,IPD_Fund_Code,Corporate_Action_Date,Distributions,Unit_Splits,Report_Date,net_distribution)"
                   + " values('" + EPF_IPD_Code + "','" + IPD_Fund_Code + "','" + Corporate_Action_Date + "','" + Distributions + "','" + Unit_Splits + "','" + ReinstatementDate + "','" + Net_Distrubutions + "')";
                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                    {

                        string Value = DB.Find_Value_exists("select * from utmc_fund_corporate_actions where Corporate_Action_Date='" + Corporate_Action_Date + "' and IPD_Fund_Code='" + IPD_Fund_Code + "'");
                        if (Value == "0")
                        {
                            mysql_comm.ExecuteNonQuery();
                            Log.Write("Sync Corporate Actions : " + IPD_Fund_Code + "-" + IPD_Fund_Code + "-" + Corporate_Action_Date + " Inserted", DateTime.Now.ToString("yyyy-MM-dd"));
                        }
                    }
                }
                mysql_conn.Close();
            }
        }

        protected void Inser_Fund_Nav_Histrocial()
        {

            string HistricalStartDate = StartDate.Text;
            HistricalStartDate = HistricalStartDate.Replace("/", "-");
            string HistricalStartDateEndDAte = EndDate.Text;
            HistricalStartDateEndDAte = HistricalStartDateEndDAte.Replace("/", "-");

            DS = CallService.Get_NAV_Per_Fund(HistricalStartDate, HistricalStartDateEndDAte);
            string Sqlconnstring = DB.GetSqlConnection();
            using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
            {
                mysql_conn.Open();
                foreach (DataRow rdr in DS.Rows)
                {
                    string EPF_IPD_Code = ConfigurationManager.AppSettings["IPD_CODE"];

                    string IPD_Fund_Code = rdr[0].ToString();

                    DateTime ForamtedDate = Convert.ToDateTime(rdr[1].ToString());

                    DateTime dt = Convert.ToDateTime(ForamtedDate);
                    string Daily_NAV_Date = dt.ToString("yyyy-MM-dd");
                    string Daily_NAV = rdr[4].ToString();
                    if (Daily_NAV == "")
                        Daily_NAV = "0.00";
                    string Daily_Unit_created = rdr[3].ToString();
                    if (Daily_Unit_created == "")
                        Daily_Unit_created = "0.00";
                    string Daily_Unit_Price = rdr[2].ToString();
                    if (Daily_Unit_Price == "")
                        Daily_Unit_Price = "0.00";

                    decimal daily_Unit_Created_EPF = 0.4M;
                    decimal Daily_NAV_EPF = 0.2M;


                    string Query = "Insert into utmc_daily_nav_fund_h("
                        + "EPF_IPD_Code, IPD_Fund_Code, Daily_NAV_Date, Daily_NAV, Report_Date,"
                           + "Daily_Unit_Created, Daily_NAV_EPF, Daily_Unit_Created_EPF, Daily_Unit_Price)"
                             + " values('" + EPF_IPD_Code + "','" + IPD_Fund_Code + "','" + Daily_NAV_Date + "','" + Daily_NAV + "','" + End_date + "','"
                             + Daily_Unit_created + "', " + " '" + Daily_NAV_EPF + "','" + daily_Unit_Created_EPF + "','" + Daily_Unit_Price + "');";



                    using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                    {

                        string Value = DB.Find_Value_exists("select * from utmc_daily_nav_fund_h where Daily_NAV_Date='" + Daily_NAV_Date + "' and IPD_Fund_Code='" + IPD_Fund_Code + "'");
                        if (Value == "0")
                        {
                            mysql_comm.ExecuteNonQuery();
                            Log.Write("UTMC_018 : " + IPD_Fund_Code + Daily_NAV_Date + "-" + Daily_NAV_EPF + " Inserted", End_date);
                        }
                    }
                }

                mysql_conn.Close();
            }
        }

        private void Insert_Holder_Account_Details_Histrocial()
        {

            try
            {
                DS = CallService.Get_Holder_Acc_Details_Histrocial(End_date);
                string Sqlconnstring = DB.GetSqlConnection();

                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();
                    foreach (DataRow rdr in DS.Rows)
                    {
                        string EPF_IPD_Code = ConfigurationManager.AppSettings["IPD_CODE"];

                        string ID_NO = rdr[0].ToString();
                        string ID_NO_OLD = rdr[1].ToString();
                        string ID_NO_2 = rdr[2].ToString();
                        string ID_NO_OLD_2 = rdr[3].ToString();
                        string ID_NO_3 = rdr[4].ToString();
                        string ID_NO_OLD_3 = rdr[5].ToString();
                        string ID_NO_4 = rdr[6].ToString();
                        string ID_NO_OLD_4 = rdr[7].ToString();
                        string ID_NO_5 = rdr[8].ToString();
                        string ID_NO_OLD_5 = rdr[9].ToString();
                        string HOLDER_NO = rdr[10].ToString();
                        string TITLE = rdr[11].ToString();
                        TITLE = TITLE.Replace("'", "");
                        string NAME_1 = rdr[12].ToString();
                        NAME_1 = NAME_1.Replace("'", "");
                        string NAME_2 = rdr[13].ToString();
                        NAME_2 = NAME_2.Replace("'", "");
                        NAME_2 = NAME_2.Replace("'", "");
                        string NAME_3 = rdr[14].ToString();
                        NAME_3 = NAME_3.Replace("'", "");
                        string NAME_4 = rdr[15].ToString();
                        NAME_4 = NAME_4.Replace("'", "");
                        string NAME_5 = rdr[16].ToString();
                        NAME_5 = NAME_5.Replace("'", "");
                        string ADDR_1 = rdr[17].ToString();
                        ADDR_1 = ADDR_1.Replace("'", "");
                        string ADDR_2 = rdr[18].ToString();
                        ADDR_2 = ADDR_2.Replace("'", "");
                        string ADDR_3 = rdr[19].ToString();
                        ADDR_3 = ADDR_3.Replace("'", "");
                        string ADDR_4 = rdr[20].ToString();
                        ADDR_4 = ADDR_4.Replace("'", "");
                        string POSTCODE = rdr[21].ToString();
                        string TEL_NO = rdr[22].ToString();
                        string REGION_CODE = rdr[23].ToString();
                        string STATE_CODE = rdr[24].ToString();
                        string COUNTRY_ISSUE = rdr[25].ToString();
                        COUNTRY_ISSUE = COUNTRY_ISSUE.Replace("'", "");
                        string COUNTRY_ISSUE1 = rdr[26].ToString();
                        COUNTRY_ISSUE1 = COUNTRY_ISSUE1.Replace("'", "");
                        string COUNTRY_ISSUE2 = rdr[27].ToString();
                        COUNTRY_ISSUE2 = COUNTRY_ISSUE2.Replace("'", "");
                        string COUNTRY_ISSUE3 = rdr[28].ToString();
                        COUNTRY_ISSUE3 = COUNTRY_ISSUE3.Replace("'", "");
                        string COUNTRY_RES = rdr[29].ToString();
                        COUNTRY_RES = COUNTRY_RES.Replace("'", "");
                        string COUNTRY_INCORP = rdr[30].ToString();
                        COUNTRY_INCORP = COUNTRY_INCORP.Replace("'", "");
                        string NATIONALITY = rdr[31].ToString();
                        string RACE = rdr[32].ToString();
                        string OCC_CODE = rdr[33].ToString();
                        string CORP_STATUS = rdr[34].ToString();
                        string BUSINESS_TYPE = rdr[35].ToString();
                        string CONTACT_PERSON = rdr[36].ToString();
                        CONTACT_PERSON = CONTACT_PERSON.Replace("'", "");
                        string POSITION_HELD = rdr[37].ToString();
                        POSITION_HELD = POSITION_HELD.Replace("'", "");
                        string ACCOUNT_TYPE = rdr[38].ToString();
                        string DIV_PYMT = rdr[39].ToString();
                        string HOLDER_CLS = rdr[40].ToString();
                        string HOLDER_IND = rdr[41].ToString();
                        string HOLDER_STATUS = rdr[42].ToString();
                        string REG_DT = rdr[43].ToString();
                        string REG_BRN = rdr[44].ToString();
                        string BIRTH_DT = rdr[45].ToString();
                        string SEX = rdr[46].ToString();
                        string TEL_NO_2 = rdr[47].ToString();
                        string AGENT_CODE = rdr[48].ToString();
                        string AGENT_TYPE = rdr[49].ToString();
                        string AGENT_ID = rdr[50].ToString();
                        string INCOME_TAX_NO = rdr[51].ToString();
                        string HAND_PHONE_NO = rdr[52].ToString();
                        string OFF_PHONE_NO = rdr[53].ToString();
                        string PRMNT_ADDR_1 = rdr[54].ToString();
                        PRMNT_ADDR_1 = PRMNT_ADDR_1.Replace("'", "");
                        string PRMNT_ADDR_2 = rdr[55].ToString();
                        PRMNT_ADDR_2 = PRMNT_ADDR_2.Replace("'", "");
                        string PRMNT_ADDR_3 = rdr[56].ToString();
                        PRMNT_ADDR_3 = PRMNT_ADDR_3.Replace("'", "");
                        string PRMNT_ADDR_4 = rdr[57].ToString();
                        PRMNT_ADDR_4 = PRMNT_ADDR_4.Replace("'", "");
                        string PRMNT_POST_CODE = rdr[58].ToString();
                        string PRMNT_REGION = rdr[59].ToString();
                        PRMNT_REGION = PRMNT_REGION.Replace("'", "");
                        string PRMNT_STATE = rdr[60].ToString();
                        PRMNT_STATE = PRMNT_STATE.Replace("'", "");
                        string SALUTATION = rdr[61].ToString();
                        SALUTATION = SALUTATION.Replace("'", "");
                        string H_ACC_TYPE = rdr[62].ToString();
                        string MARITAL = rdr[63].ToString();
                        string NO_OF_DPNDNT = rdr[64].ToString();
                        if (NO_OF_DPNDNT == "") NO_OF_DPNDNT = "0";
                        string RCV_MATERIAL = rdr[65].ToString();
                        RCV_MATERIAL = RCV_MATERIAL.Replace("'", "");
                        string MATERIAL_LAN = rdr[66].ToString();
                        MATERIAL_LAN = MATERIAL_LAN.Replace("'", "");
                        string FIELD_1 = rdr[67].ToString();
                        string FIELD_2 = rdr[68].ToString();                  
                        string DOWNLOAD_IND = rdr[90].ToString();
                        string IDENTITY_IND = rdr[91].ToString();
     


                        string Query = "INSERT INTO ma_holder_reg_emis_h ( ID_NO, "
                                     + " ID_NO_OLD,"
                                     + " ID_NO_2,ID_NO_OLD_2,ID_NO_3,ID_NO_OLD_3,ID_NO_4,ID_NO_OLD_4,ID_NO_5,ID_NO_OLD_5,HOLDER_NO,TITLE,NAME_1,NAME_2,NAME_3,NAME_4,NAME_5,ADDR_1,ADDR_2, "
                                    + " ADDR_3,ADDR_4,POSTCODE,TEL_NO,REGION_CODE,STATE_CODE,COUNTRY_ISSUE,COUNTRY_ISSUE1,COUNTRY_ISSUE2,COUNTRY_ISSUE3,COUNTRY_RES,COUNTRY_INCORP,NATIONALITY, "
                                    + " RACE,OCC_CODE,CORP_STATUS,BUSINESS_TYPE,CONTACT_PERSON,POSITION_HELD,ACCOUNT_TYPE,DIV_PYMT,HOLDER_CLS,HOLDER_IND,HOLDER_STATUS,REG_DT,REG_BRN,BIRTH_DT, "
                                    + " SEX,TEL_NO_2,AGENT_CODE,AGENT_TYPE,AGENT_ID,INCOME_TAX_NO,HAND_PHONE_NO,OFF_PHONE_NO,PRMNT_ADDR_1,PRMNT_ADDR_2,PRMNT_ADDR_3,PRMNT_ADDR_4,PRMNT_POST_CODE,"
                                    + " PRMNT_REGION,PRMNT_STATE,SALUTATION,H_ACC_TYPE,MARITAL,NO_OF_DPNDNT,RCV_MATERIAL,MATERIAL_LAN,FIELD_1,FIELD_2,DOWNLOAD_IND,IDENTITY_IND) "
                                    + " VALUES ('" + ID_NO + "','" + ID_NO_OLD + "','" + ID_NO_2 + "','" + ID_NO_OLD_2 + "','" + ID_NO_3 + "','" + ID_NO_OLD_3 + "','" + ID_NO_4 + "','" + ID_NO_OLD_4 + "',"
                                    + " '" + ID_NO_5 + "','" + ID_NO_OLD_5 + "','" + HOLDER_NO + "','" + TITLE + "','" + NAME_1 + "','" + NAME_2 + "','" + NAME_3 + "','" + NAME_4 + "','" + NAME_5 + "',"
                                    + " '" + ADDR_1 + "','" + ADDR_2 + "','" + ADDR_3 + "','" + ADDR_4 + "','" + POSTCODE + "','" + TEL_NO + "','" + REGION_CODE + "','" + STATE_CODE + "','" + COUNTRY_ISSUE + "',"
                                    + " '" + COUNTRY_ISSUE1 + "','" + COUNTRY_ISSUE2 + "','" + COUNTRY_ISSUE3 + "','" + COUNTRY_RES + "','" + COUNTRY_INCORP + "','" + NATIONALITY + "','" + RACE + "','" + OCC_CODE + "','" + CORP_STATUS + "',"
                                    + " '" + BUSINESS_TYPE + "','" + CONTACT_PERSON + "','" + POSITION_HELD + "','" + ACCOUNT_TYPE + "','" + DIV_PYMT + "','" + HOLDER_CLS + "','" + HOLDER_IND + "','" + HOLDER_STATUS + "','" + REG_DT + "',"
                                    + " '" + REG_BRN + "','" + BIRTH_DT + "','" + SEX + "','" + TEL_NO_2 + "','" + AGENT_CODE + "','" + AGENT_TYPE + "','" + AGENT_ID + "','" + INCOME_TAX_NO + "','" + HAND_PHONE_NO + "',"
                                    + " '" + OFF_PHONE_NO + "','" + PRMNT_ADDR_1 + "','" + PRMNT_ADDR_2 + "','" + PRMNT_ADDR_3 + "','" + PRMNT_ADDR_4 + "','" + PRMNT_POST_CODE + "','" + PRMNT_REGION + "','" + PRMNT_STATE + "','" + SALUTATION + "',"
                                    + " '" + H_ACC_TYPE + "','" + MARITAL + "','" + NO_OF_DPNDNT + "','" + RCV_MATERIAL + "','" + MATERIAL_LAN + "','" + FIELD_1 + "','" + FIELD_2 + "','" + DOWNLOAD_IND + "','" + IDENTITY_IND + "')";

                        using (MySqlCommand mysql_comm = new MySqlCommand(Query, mysql_conn))
                        {


                            string Value = DB.Find_Value_exists("select * from ma_holder_reg_emis_h where ID_NO='" + ID_NO + "' and HOLDER_NO='" + HOLDER_NO + "'");
                            if (Value == "0")
                                mysql_comm.ExecuteNonQuery();
                            Log.Write("Holder Acc Details Inserted :", End_date);
                        }
                    }

                    mysql_conn.Close();
                }
            }
            catch (Exception ex)
            {
                Log.Write("Insert Holder Acc Details failed :" + ex.ToString(), End_date);
            }
        }


        private void Process_UTMC_Purge_Histrocial()
        {
            string Mysql = "";
            string Sqlconnstring = GetSqlConnection();
            try
            {
                Mysql = "SET SQL_SAFE_UPDATES = 0; "
                            + "DELETE   from  utmc_member_investment_h; "
                            + "DELETE   from  ma_holder_reg_emis_h; "
                            + "DELETE   from  utmc_daily_nav_fund_h; "
                            + " ";

                string ReportDt = End_date;
                using (MySqlConnection mysql_conn = new MySqlConnection(Sqlconnstring))
                {
                    mysql_conn.Open();


                    using (MySqlCommand mysql_comm = new MySqlCommand(Mysql, mysql_conn))
                    {
                        mysql_comm.ExecuteNonQuery();
                        Log.Write("UTMC_Purge : " + Mysql + " - SUCCESSFULLY", End_date);
                    }


                    mysql_conn.Close();
                }

            }
            catch (Exception ex)
            {
                Log.Write("UTMC_Purge : " + Mysql + " - UNSUCCESSFULLY", End_date);
                MessageBox.Show(ex.Message);

            }

        }



        private void btn_import_Click_fn_Histrocial()
        {

            string EndDateforPMS = EndDate.Text; ;
            EndDateforPMS = EndDateforPMS.Replace("/", "");
            EndDateforPMS = EndDateforPMS.Replace("-", "");

            string StartDateforPMS = StartDate.Text; ;
            StartDateforPMS = StartDateforPMS.Replace("/", "");
            StartDateforPMS = StartDateforPMS.Replace("-", "");


            string ImportFilePath = ConfigurationManager.AppSettings["Import_file_path"];

            string ExportFilePath = ConfigurationManager.AppSettings["Export_file_path"];


            string str = "";
            string ReadLine = "";
            string filePath = @"" + ExportFilePath + "";
            int ColumnValue = 0;

            System.IO.DirectoryInfo dr = new System.IO.DirectoryInfo(filePath);
            System.IO.FileInfo[] files = dr.GetFiles("*.txt");

            string destFolder = ExportFilePath + "\\history_Full\\" + StartDateforPMS+"-"+ EndDateforPMS + "\\";
            string destFolderFile = "";


            string Query = "UPDATE trailer_file_h SET number_of_row = 0";

            UTMC02_Update_for_Row_and_ColumnValue(Query);


            foreach (System.IO.FileInfo fie in files)
            {
                str = fie.Name;


                Log.Write(DateTime.Now.ToString() + " - start export file: " + str, End_date);

                destFolderFile = destFolder + str;

                string s = str;
                string strRptCode = s.Split('_')[0].ToString();

                System.IO.StreamReader sr = new System.IO.StreamReader(filePath + "\\" + str);

                var line = sr.ReadToEnd().Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                int Rownumber = line.Length;


                #region  update_column_Count

                for (int j = 0; j < line.Length;)
                {
                    ReadLine = line[j];
                    string[] columm1 = ReadLine.Split('|');
                    ColumnValue = columm1.Length;
                    break;
                }
                #endregion

                # region UpdateRow and Column

                string Checksum = GenerateTrailerFile(fie.FullName);

                //if (strRptCode == "FundsInfo")
                //    UTMC02_Update("FundsInfo", Rownumber, ColumnValue, Checksum);

                if (strRptCode == "FundsDailyNAVPrice")
                    UTMC02_Update("FundsDailyNAVPrice", Rownumber, ColumnValue, Checksum);

                if (strRptCode == "FundsIncomeDistribution")
                    UTMC02_Update("FundsIncomeDistribution", Rownumber, ColumnValue, Checksum);

                //if (strRptCode == "FundsSize")
                //    UTMC02_Update("FundsSize", Rownumber, ColumnValue, Checksum);

                if (strRptCode == "MemberUTMCFullHoldings")
                    UTMC02_Update("MemberUTMCFullHoldings", Rownumber, ColumnValue, Checksum);

                #endregion

                sr.Close();
                sr = null;

                string HeaderFundsDailyNAVPrice = @"SELECT 'EPF_IPD_CODE','NUMBER_OF_ROW','NUMBER_OF_COLUMN','CHECKSUM' UNION ALL ";

                string UTMC02_query = HeaderFundsDailyNAVPrice + "SELECT epf_ipd_code,number_of_row,number_of_column,checksum FROM trailer_file_h where  report_name='" + strRptCode + "'";
                Write_Export_Data(UTMC02_query, strRptCode, "Trailer");


            }


            bool exists = System.IO.Directory.Exists(destFolder);

            if (!exists)
                System.IO.Directory.CreateDirectory(destFolder);


            System.IO.DirectoryInfo dr1 = new System.IO.DirectoryInfo(filePath);
            System.IO.FileInfo[] files1 = dr.GetFiles("*.txt");


            foreach (System.IO.FileInfo fie in files1)
            {
                destFolderFile = destFolder + fie;


                fie.CopyTo(destFolderFile, true);

                Log.Write(DateTime.Now.ToString() + " - copy export file to history folder: " + destFolderFile, End_date);
                fie.Delete();

            }
        }


    }
}



